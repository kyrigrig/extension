<?php
// Heading
$_['homework_href']        = 'Homework';
$_['exams_href']           = 'Exams';
$_['schedule_href']        = 'Schedule';
$_['more_href']            = 'More';

$_['homework_menu']        = 'Homework';
$_['exams_menu']           = 'Exams';
$_['schedule_menu']        = 'Schedule';
$_['more_menu']            = 'More';
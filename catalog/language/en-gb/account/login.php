<?php
// Heading
$_['heading_title']                 = 'Login';

//Texts
$_['login_button']                  = 'LOGIN';
$_['forgot_password']               = 'Forgot your password?';
$_['reset_password']                = 'Reset';
$_['input_phone']                   = 'Phone';
$_['input_password']                = 'Password';

//Errors messages
$_['error_login']                   = 'Bad user name or password';
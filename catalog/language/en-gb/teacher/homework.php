<?php
// Heading
$_['heading_title']             = 'Homework';

//Inside Modals
$_['heading_add_homework_title']    = 'Homework';
$_['modal_add_study_book_title']    = 'Book';
//Modal Texts
$_['modal_close']                   = 'Close';
$_['modal_add_homework_name']       = 'Name';
$_['modal_add_homework_page']       = 'Page';
$_['modal_add_homework_comments']   = 'Comments';
$_['modal_add_homework_book']       = 'Book';
$_['add_homework_btn_title']        = 'ADD';
$_['save_homework_btn_title']       = 'SAVE';
$_['edit_homework_btn_title']       = 'EDIT';
$_['delete_homework_delete_title']  = 'Delete';
$_['cancel_homework_title']         = 'Cancel';

//Text
$_['modal_courses']             = 'Courses';
$_['course_text']               = 'Course';
$_['choice_text']               = 'Select';
$_['pick_day_text']             = 'Pick a Date';
$_['no_homework_title']         = 'There are no exercises';
$_['add_homework_title']        = 'Add Exercise';
$_['homework_title']            = 'Homework: ';
$_['classroom_title']           = 'Classroom: ';
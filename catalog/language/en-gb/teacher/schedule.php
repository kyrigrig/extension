<?php
// Heading
$_['heading_title']             = 'Schedule';

//Text
$_['course_text']               = 'Course';
$_['all_text']                  = 'All';
$_['modal_courses']             = 'Courses';
$_['modal_close']               = 'Close';
$_['room_class_text']           = 'Roomclass:';

//Weekdays
$_['monday_text']               = 'Monday';
$_['tuesday_text']              = 'Tuesday';
$_['wednesday_text']            = 'Wednesday';
$_['thursday_text']             = 'Thursday';
$_['friday_text']               = 'Friday';
$_['saturday_text']             = 'Saturday';
$_['sunday_text']               = 'Sunday';
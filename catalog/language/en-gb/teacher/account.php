<?php
// Heading
$_['heading_title']             = 'Account';

$_['account_last_name']         = 'Last name';
$_['account_first_name']        = 'First name';
$_['account_phone_number']      = 'Phone number';
$_['account_email']             = 'E-mail';
$_['account_change_password']   = 'Change password';

$_['account_old_password']          = 'Old Password';
$_['account_new_password']          = 'New Password';
$_['account_confirm_password']      = 'Confirm Password';

//Modal Texts
$_['account_change_password_title'] = 'Change Password';
$_['modal_close']                   = 'Close';

//Errors change password
$_['incorrect_password']            = 'Could not change password: Incorrect password';

//Success change password
$_['change_correct_password']       = 'Password changed successfully!';
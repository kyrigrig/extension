<?php
// Heading
$_['heading_title']             = 'Exams';
//Inside Modals
$_['heading_add_exam_title']                = 'Exam';
$_['heading_add_study_title']               = 'Material';
$_['modal_add_study_book_title']            = 'Book';
$_['modal_add_study_chapter_exam_name']     = 'Chapter';
$_['modal_add_study_section_name']          = 'Section';
$_['modal_add_study_comments_title']        = 'Comments';
//Modal Texts
$_['modal_close']                       = 'Close';
$_['modal_add_exam_name']               = 'Exam Name';
$_['add_exam_btn_title']                = 'ADD EXAM';
$_['add_material_btn_title']            = 'ADD MATERIAL';
$_['edit_exam_more_btn_title']          = 'more';
$_['edit_exam_delete_title']            = 'Delete';
$_['edit_exam_material_delete_title']   = 'Delete';
$_['edit_exam_score_title']             = 'Add Scores';
$_['edit_exam_cancel_title']            = 'Cancel';
$_['edit_exam_material_cancel_title']   = 'Cancel';
//Text
$_['modal_courses']             = 'Courses';
$_['course_text']               = 'Course';
$_['choice_text']               = 'Select';
$_['pick_day_text']             = 'Pick a Date';
$_['all_text']                  = 'All';
$_['add_exam_title']            = 'Add Exam';
$_['no_exams_title']            = 'No exams for picked date';
$_['add_exam_ili_title']        = 'Add Material';
$_['exams_title']               = 'Exams: ';
$_['classroom_title']           = 'Classroom: ';
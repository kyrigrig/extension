<?php
// Heading
$_['heading_title']             = 'Change language';

//Text
$_['explain_msg_switch_lang']   = 'Επίλεξε την γλώσσα της επιθυμίας σου';
$_['english_txt']               = 'English';
$_['greek_txt']                 = 'Ελληνικά (Greek)';
$_['save_btn']                  = 'SAVE';
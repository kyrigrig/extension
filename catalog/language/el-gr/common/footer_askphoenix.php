<?php
// Heading
$_['homework_href']        = 'Εργασίες';
$_['exams_href']           = 'Διαγωνίσματα';
$_['schedule_href']        = 'Πρόγραμμα';
$_['more_href']            = 'Περισσότερα';

$_['homework_menu']        = 'Εργασίες';
$_['exams_menu']           = 'Διαγωνίσματα';
$_['schedule_menu']        = 'Πρόγραμμα';
$_['more_menu']            = 'Περισσότερα';
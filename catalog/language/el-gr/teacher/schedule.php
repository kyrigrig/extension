<?php
// Heading
$_['heading_title']             = 'Πρόγραμμα';

//Text
$_['course_text']               = 'Μάθημα';
$_['all_text']                  = 'Όλα';
$_['modal_courses']             = 'Μαθήματα';
$_['modal_close']               = 'Κλήσιμο';
$_['room_class_text']           = 'Αίθουσα:';

//Weekdays
$_['monday_text']               = 'Δευτέρα';
$_['tuesday_text']              = 'Τρίτη';
$_['wednesday_text']            = 'Τετάρτη';
$_['thursday_text']             = 'Πέμπτη';
$_['friday_text']               = 'Παρασκευή';
$_['saturday_text']             = 'Σάββατο';
$_['sunday_text']               = 'Κυριακή';
<?php
// Heading
$_['heading_title']             = 'Εργασίες';
//Inside Modals
$_['heading_add_homework_title']    = 'Εργασία';
$_['modal_add_study_book_title']    = 'Βιβλίο';
//Modal Texts
$_['modal_close']                   = 'Κλήσιμο';
$_['modal_add_homework_name']       = 'Όνομα';
$_['modal_add_homework_page']       = 'Σελίδα';
$_['modal_add_homework_comments']   = 'Σχόλια';
$_['modal_add_homework_book']       = 'Βιβλίο';
$_['add_homework_btn_title']        = 'ΠΡΟΣΘΗΚΗ';
$_['save_homework_btn_title']       = 'ΑΠΟΘΗΚΕΥΣΗ';
$_['edit_homework_btn_title']       = 'ΕΠΕΞΕΡΓΑΣΙΑ';
$_['delte_homework_delete_title']   = 'ΔΙΑΓΡΑΦΗ';
$_['cancel_homework_title']         = 'ΑΚΥΡΟ';

//Text
$_['modal_courses']             = 'Μαθήματα';
$_['course_text']               = 'Μάθημα';
$_['choice_text']               = 'Επιλογή';
$_['pick_day_text']             = 'Επιλογή ημερομηνίας';
$_['no_homework_title']         = 'Δεν υπάρχουν εργασίες';
$_['add_homework_title']        = 'Προσθήκη Εργασίας';
$_['homework_title']            = 'Εργασία: ';
$_['classroom_title']           = 'Αίθουσα: ';
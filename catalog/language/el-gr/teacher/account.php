<?php
// Heading
$_['heading_title']             = 'Ο Λογαριασμός μου';

$_['account_last_name']         = 'Επώνυμο';
$_['account_first_name']        = 'Όνομα';
$_['account_phone_number']      = 'Κινητό';
$_['account_email']             = 'E-mail';
$_['account_change_password']   = 'Αλλαγή κωδικού';

$_['account_old_password']          = 'Παλιός κωδικός';
$_['account_new_password']          = 'Νέος κωδικός';
$_['account_confirm_password']      = 'Επιβεβαίωση νέου κωδικού';

//Modal Texts
$_['account_change_password_title'] = 'Αλλαγή κωδικού';


//Errors change password
$_['incorrect_password']            = 'Δεν μπορείτε να αλλάξετε κωδικό: Λάθος κωδικός!';

//Success change password
$_['change_correct_password']       = 'Ο κωδικός έχει αλλάξει με επιτυχία!';
<?php
// Heading
$_['heading_title']             = 'Διαγωνίσματα';
//Inside Modals
$_['heading_add_exam_title']                = 'Διαγώνισμα';
$_['heading_add_study_title']               = 'Ύλη';
$_['modal_add_study_book_title']            = 'Βιβλίο';
$_['modal_add_study_chapter_exam_name']     = 'Κεφάλαιο';
$_['modal_add_study_section_name']          = 'Ενότητα';
$_['modal_add_study_comments_title']        = 'Σχόλια';
//Modal Texts
$_['modal_close']                       = 'Κλήσιμο';
$_['modal_add_exam_name']               = 'Ονομασία';
$_['add_exam_btn_title']                = 'ΠΡΟΣΘΗΚΗ';
$_['add_material_btn_title']            = 'ΠΡΟΣΘΗΚΗ';
$_['edit_exam_more_btn_title']          = 'Περισσότερα';
$_['edit_exam_delete_title']            = 'Αφαίρεση';
$_['edit_exam_material_delete_title']   = 'Αφαίρεση';
$_['edit_exam_score_title']             = 'Προσθήκη Βαθμολογίας';
$_['edit_exam_cancel_title']            = 'Άκυρο';
$_['edit_exam_material_cancel_title']   = 'Άκυρο';
//Text
$_['modal_courses']             = 'Μαθήματα';
$_['course_text']               = 'Μάθημα';
$_['choice_text']               = 'Επιλογή';
$_['pick_day_text']             = 'Επιλογή Ημερομηνίας';
$_['all_text']                  = 'Όλα';
$_['add_exam_title']            = 'Προσθήκη Διαγωνίσματος';
$_['no_exams_title']            = 'Δεν υπάρχουν διαγωνίσματα';
$_['add_exam_ili_title']        = 'Προσθήκη Ύλης';
$_['exams_title']               = 'Διαγωνίσματα ';
$_['classroom_title']           = 'Αίθουσα: ';
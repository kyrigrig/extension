<?php
// Heading
$_['heading_title']                 = 'Σύνδεση';

//Texts
$_['login_button']                  = 'ΣΥΝΔΕΣΗ';
$_['forgot_password']               = 'Ξέχασες τον κωδικό σου;';
$_['reset_password']                = 'Επαναφορά';
$_['input_phone']                   = 'Τηλέφωνο';
$_['input_password']                = 'Κωδικός';

//Errors messages
$_['error_login']                   = 'Το κινητό ή/και ο κωδικός πρόσβασης δεν είναι έγκυρα';

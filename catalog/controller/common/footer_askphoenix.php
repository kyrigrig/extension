<?php
class ControllerCommonFooterAskphoenix extends Controller {
	public function index() {
        //sessions
        if($this->session->data['token']) {
            $token = $this->session->data['token'];
            $data['token'] = $token;
        }

        $this->load->language('common/footer_askphoenix');

		$data['homework_href'] = $this->url->link('teacher/homework');
		$data['exams_href'] = $this->url->link('teacher/exams');
		$data['schedule_href'] = $this->url->link('teacher/schedule');
		$data['more_href'] = $this->url->link('teacher/more');
		
		return $this->load->view('common/footer_askphoenix', $data);
	}
}
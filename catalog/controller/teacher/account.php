<?php
class ControllerTeacherAccount extends Controller
{
    public function index()
    {
        $asx_class = new Askphoenix();
        $auth = $asx_class->asx_check_token($this->session->data['token']);
        if (!empty($auth['asx_redirect'])) {
            $this->response->redirect($this->url->link($auth['asx_redirect'], '', true));
        }

        $args = array(
            'token' => $this->session->data['token']
        );

        //initiate values
        $this->load->language('teacher/account');
        //error msgs
        $data['incorrect_password']         = $this->language->get('incorrect_password');
        $data['change_correct_password']    = $this->language->get('change_correct_password');
        $data = array();
        $data['current_url']            = 'index.php?route=teacher/account';
        $data['previous_current_url']   = 'index.php?route=teacher/more';

        if(isset($_POST['submit_change_password'])) {
            $data_passwords = array();
            $data_passwords['newPassword'] = $_POST['newPassword'];
            $data_passwords['oldPassword'] = $_POST['oldPassword'];
            $data_string = json_encode($data_passwords);
            $url_change_password = "https://api.askphoenix.gr/api/account/change-password";
            $url_change_password_results = $asx_class->asx_curl_post($url_change_password, $data_string, $args, '');
            ////var_dump($url_change_password_results);
            if($url_change_password_results) {
                if(!empty($url_change_password_results['asx_message'])) {
                    $data['asx_code'] = $url_change_password_results['asx_code'];
                    $data['asx_message'] = $data['incorrect_password']; //$url_change_password_results['asx_message'];
                }
//                else { //success message
//                    $data['asx_code'] = $url_change_password_results['asx_code'];
//                    $data['asx_message'] = $data['change_correct_password']; //$url_change_password_results['asx_message'];
//                }
            }
        }

        $url_account = "https://api.askphoenix.gr/api/account/me";
        $accounts_results = $asx_class->asx_general_curl($url_account, $data_string = '', $args, '');
            //var_dump($accounts_results);
        if (!empty($accounts_results)) {
            $data['account_info'] = array(
                'first_name'        => $accounts_results['results']->firstName,
                'last_name'         => $accounts_results['results']->lastName,
                'phone_number'      => $accounts_results['results']->aspNetUser->phoneNumber,
                'email_confirmed'   => (!empty($accounts_results['results']->aspNetUser->emailConfirmed)) ? 'false' : 'un-confirmed'
            );
        }

        $this->document->setTitle($this->language->get('heading_title'));

        $data['options'][] = array(
            'title_option' => $data['course_text'],
            'menu_option'   =>  '#'
        );

        $data['header'] = $this->load->controller('common/header_askphoenix');
        $data['footer'] = $this->load->controller('common/footer_askphoenix');

        $this->response->setOutput($this->load->view('teacher/account', $data));
    }
}
<?php
class ControllerTeacherLanguage extends Controller
{
    public function index()
    {
        $asx_class = new Askphoenix();
        $auth = $asx_class->asx_check_token($this->session->data['token']);
        if (!empty($auth['asx_redirect'])) {
            $this->response->redirect($this->url->link($auth['asx_redirect'], '', true));
        }

        $args = array(
            'token' => $this->session->data['token']
        );

        //initiate values
        $this->load->language('teacher/language');
        //error msgs
//        $data['incorrect_password']         = $this->language->get('incorrect_password');
//        $data['change_correct_password']    = $this->language->get('change_correct_password');
        $data = array();
        $data['current_url']            = 'index.php?route=teacher/language';
        $data['previous_current_url']   = 'index.php?route=teacher/more';





        $this->document->setTitle($this->language->get('heading_title'));

        $data['options'][] = array(
            'title_option' => $data['course_text'],
            'menu_option'   =>  '#'
        );

        $data['header'] = $this->load->controller('common/header_askphoenix');
        $data['footer'] = $this->load->controller('common/footer_askphoenix');

        $this->response->setOutput($this->load->view('teacher/language', $data));
    }
}
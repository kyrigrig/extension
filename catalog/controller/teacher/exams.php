<?php
class ControllerTeacherExams extends Controller
{
    public function index()
    {
        $asx_class = new Askphoenix();
        $auth = $asx_class->asx_check_token($this->session->data['token']);
        if(!empty($auth['asx_redirect'])) {
            $this->response->redirect($this->url->link($auth['asx_redirect'], '', true));
        }

        $args = array(
            'token' => $this->session->data['token']
        );

        //initiate values
        $this->load->language('teacher/exams');
        $data = array();
        $data['current_url'] = 'index.php?route=teacher/exams';

        if(empty($_GET['id'])) { //reset all EXAM sessions if id does not exist
            //$asx_class->unsetSessionsExams();
            unset($this->session->data['course_id']);
            unset($this->session->data['course_name']);
            unset($this->session->data['date_id']);
            unset($this->session->data['lecture_id']);
            unset($this->session->data['date_format']);
            unset($this->session->data['class_room_id']);
            unset($this->session->data['class_room_name']);
            unset($this->session->data['exam_lecture_start_date_time']);
            unset($this->session->data['exam_lecture_end_date_time']);
        }
        else if(empty($_GET['lecture_id'])) {
            unset($this->session->data['date_id']);
            unset($this->session->data['lecture_id']);
            unset($this->session->data['date_format']);
            unset($this->session->data['class_room_id']);
            unset($this->session->data['class_room_name']);
            unset($this->session->data['exam_lecture_start_date_time']);
            unset($this->session->data['exam_lecture_end_date_time']);
        }

        if(!empty($_GET['id'])) {
            $this->session->data['course_id'] = $this->request->get['id'];
        }
        if(!empty($_GET['course_name'])) {
            $this->session->data['course_name'] = $this->request->get['course_name'];
        }
        if(!empty($_GET['date_id'])) {
            $this->session->data['date_id'] = $this->request->get['date_id'];
        }
        if(!empty($_GET['lecture_id'])) { //pick a date
            $this->session->data['lecture_id'] = $this->request->get['lecture_id'];
        }
        if(!empty($_GET['date_format'])) { //date from pick a date
            $this->session->data['date_format'] = $this->request->get['date_format'];
        }
        //END initiate values
        //var_dump($this->session->data);

        //DELETE CALLS
        if(isset($this->request->get['delete']) && !empty($this->request->get['delete'])) { //DELETE Exam
            //echo $this->request->get['delete'];
            $delete_exam_url = "https://api.askphoenix.gr/api/exam/".$this->request->get['delete'];
            //$data = array('id' => $this->request->get['delete']);
            $delete_exam = $asx_class->asx_curl_delete($delete_exam_url,  '' , $args, '');
            //var_dump($delete_exam);
            //message to admin
        }

        if(isset($this->request->get['delete_material']) && !empty($this->request->get['delete_material'])) {
            $delete_exam_material_url = "https://api.askphoenix.gr/api/material/".$this->request->get['delete_material'];
            $res_delete_exam_material = $asx_class->asx_curl_delete($delete_exam_material_url,  '' , $args, '');
            //var_dump($delete_exam);
            //message to admin
        }

        //POST CALLS
        if($this->request->post['submit_form'] == 1) { //POST Add New Exam
            $url_add_exam = "https://api.askphoenix.gr/api/exam/";
            $data_exam = array();
            $data_exam["comments"] = null;
            $data_exam["id"] = null;
            $data_exam["materials"] = null;
            $data_exam["name"] = $this->request->post['exam_name'];
            $data_exam["lecture"]["id"] = (!empty($this->session->data['lecture_id'])) ? $this->session->data['lecture_id']  : '';
            $data_exam["lecture"]["startDateTime"] =(!empty($this->session->data['lecture_start_date_time'])) ? $this->session->data['lecture_start_date_time'] : '';
            $data_exam["lecture"]["endDateTime"] = (!empty($this->session->data['lecture_end_date_time'])) ? $this->session->data['lecture_end_date_time'] : '';
            $data_exam["lecture"]["status"] = (!empty($this->session->data['lecture_status'])) ? $this->session->data['lecture_status'] : '';
            $data_exam["lecture"]["createdBy"] = 0;
            $data_exam["lecture"]["exercises"] = [];
            $data_exam["lecture"]["classroom"]["id"] = (!empty($this->session->data['class_room_id'])) ? $this->session->data['class_room_id'] : '';
            $data_exam["lecture"]["classroom"]["name"] = (!empty($this->session->data['class_room_name'])) ? $this->session->data['class_room_name'] : '';
            $data_exam["lecture"]["course"]["firstDate"] = (!empty($this->session->data['course_first_date'])) ?$this->session->data['course_first_date'] : '';
            $data_exam["lecture"]["course"]["group"] = (!empty($this->session->data['course_group'])) ? $this->session->data['course_group'] : '';
            $data_exam["lecture"]["course"]["id"] = (!empty($this->session->data['course_id'])) ? $this->session->data['course_id'] : '';
            $data_exam["lecture"]["course"]["lastDate"] = (!empty($this->session->data['course_last_date'])) ?$this->session->data['course_last_date'] : '';
            $data_exam["lecture"]["course"]["level"] = (!empty($this->session->data['course_level'])) ? $this->session->data['course_level'] : '';
            $data_exam["lecture"]["course"]["name"] = (!empty($this->session->data['course_name'])) ? $this->session->data['course_name'] : '';
            $data_exam["lecture"]["course"]["subCourse"] = (!empty($this->session->data['course_sub_course'])) ? $this->session->data['course_sub_course'] : '';
            $data_string = json_encode($data_exam);
            $lectures_exams = $asx_class->asx_curl_post($url_add_exam, $data_string, $args, '');
            if($lectures_exams) {
                $this->session->data[$this->session->data['lecture_id']]['exam_name'] =  $lectures_exams['results']->name; //Display Name of the Exam //only one exam - pick per day
            }
        } //end submit exam

        if($this->request->post['submit_material_form'] == 1) { //POST add Material
            $url_add_material = "https://api.askphoenix.gr/api/material/";
            $data_material = array();
            $data_material["book"]['id'] = $this->request->post['book_id'];
            $data_material["book"]['name'] = $this->request->post['name'];
            $data_material["chapter"] = $this->request->post['chapter'];
            $data_material["comments"] = $this->request->post['comments'];
            $data_material["section"] = $this->request->post['section'];
            $data_material["exam"]['id'] = $this->request->post['exam_id']; //exam id
            $data_material["exam"]['name'] = $this->request->post['course_name'];
            $data_string = json_encode($data_material);
//            var_dump($data_material);
            $res_materials = $asx_class->asx_curl_post($url_add_material, $data_string, $args, '');
            //var_dump($res_materials);
            if($res_materials) {
                //if material posted successfully, thus display
                //$url_dis_material = "https://api.askphoenix.gr/api/material/".$res_materials['results']->id;
            }
        } //end submit add material

        $url_menu_exams = "https://api.askphoenix.gr/api/course";
        $exams_modal = $asx_class->asx_general_curl($url_menu_exams, $data_string = '', $args, '');
        $data['asx_results'] = $exams_modal['results'];
        if(!empty($_GET['id']) || !empty($this->session->data['course_id'])) { //get all lecture Dates
            $course_id = $_GET['id'] = $this->session->data['course_id'];
            $data['pick_a_day'] = $this->language->get('pick_day_text');
            $url_menu_exams = "https://api.askphoenix.gr/api/course/".$course_id."/lecture/";
            $lectures_modal = $asx_class->asx_general_curl($url_menu_exams, $data_string = '', $args, '');
            //var_dump($lectures_modal);
            if($lectures_modal) {
                $data['asx_lectures_results'] = $lectures_modal['results'];
            } //end if lecture modal
        } //end if $_GET not empty course


        if(!empty($_GET['lecture_id'])) { //Get Info from lecture
            $lecture_id = $_GET['lecture_id'];
            $data['no_exams_title'] = $this->language->get('no_exams_title');
            $data['exams_title'] = $this->language->get('exams_title');
            $data['heading_add_study_title'] = $this->language->get('heading_add_study_title');

            $url_lectures_exams = "https://api.askphoenix.gr/api/lecture/".$lecture_id."/exam";
            $lectures_exams = $asx_class->asx_general_curl($url_lectures_exams, $data_string = '', $args, '');
//            var_dump($lectures_exams['results']);
            if (!empty($lectures_exams['results'])) {
                if($lectures_exams['results'][0]->materials) {
                    foreach ($lectures_exams['results'][0]->materials as $material_item) {
                        $materials_arr['exam_materials'][] = array(
                            'material_id'           => $material_item->id,
                            'material_chapter'      => $material_item->chapter,
                            'material_section'      => $material_item->section,
                            'material_comments'     => $material_item->comments,
                            'material_book_id'      => $material_item->book->id,
                            'material_book_name'    => $material_item->book->name,
                        );
                        //var_dump($materials_arr);
                        $data['materials'] = $materials_arr['exam_materials'];
                    }
                }
//                $this->session->data[$this->request->get['lecture_id']]['exam_name'] = $lectures_exams['results'][0]->name;
//                $this->session->data[$this->request->get['lecture_id']]['exam_id'] = $lectures_exams['results'][0]->id;

                $exam_name = $lectures_exams['results'][0]->name;
                $exam_id = $lectures_exams['results'][0]->id;
                $data['asx_lectures_exams_results'] = $lectures_exams['results'][0]->name;
                ////also GET Study Books if lecture_id - exam_name, exists
                $url_books = "https://api.askphoenix.gr/api/course/".$this->session->data['course_id']."/book";
                $study_books = $asx_class->asx_general_curl($url_books, $data_string = '', $args, '');
                if($study_books) {
                    foreach($study_books['results'] as $book_item) {
                        $study_books['exam_books'][]  =   array (
                            'exam_study_books_id'   => $book_item->id,
                            'exam_study_books_name' => $book_item->name,
                        );
                    } //end foreach loop
                    $data['exam_study_books'] = $study_books['exam_books'];
                }
            } else {
                $data['asx_lectures_exams_results'] = $data['no_exams_title'];
            }
        } //end if $_GET not empty lecture_id

        if(!empty($_GET['date_format'])) { //get Course Data, when pick a Date
            $url_lecture_info = "https://api.askphoenix.gr/api/lecture/".$this->session->data['lecture_id'];
            $lectures_info = $asx_class->asx_general_curl($url_lecture_info, $data_string = '', $args, '');
            if($lectures_info) {
                $data['asx_lectures_info_results'] = $lectures_info['results'];
                ////set values to sessions
                ////classroom info
                $this->session->data['class_room_id'] = $data['asx_lectures_info_results']->classroom->id;
                $this->session->data['class_room_name'] = $data['asx_lectures_info_results']->classroom->name;
                ////course Info
                $this->session->data['course_id'] = $data['asx_lectures_info_results']->course->id;
                $this->session->data['course_name'] = (!empty($this->session->data['course_name'])) ? $this->session->data['course_name'] : $data['asx_lectures_info_results']->course->name;
                $this->session->data['course_sub_course'] = $data['asx_lectures_info_results']->course->subCourse;
                $this->session->data['course_level'] = $data['asx_lectures_info_results']->course->level;
                $this->session->data['course_group'] = $data['asx_lectures_info_results']->course->group;
                $this->session->data['course_first_date'] = $data['asx_lectures_info_results']->course->firstDate;
                $this->session->data['course_last_date'] = $data['asx_lectures_info_results']->course->lastDate;
                ////general
                $this->session->data['lecture_start_date_time'] = $data['asx_lectures_info_results']->startDateTime;
                $this->session->data['lecture_end_date_time'] = $data['asx_lectures_info_results']->endDateTime;
                $this->session->data['lecture_status'] = $data['asx_lectures_info_results']->status;
                $this->session->data['lecture_created_by'] = $data['asx_lectures_info_results']->createdBy;
            }
        }

        //// Display Materials
        if(!empty($_GET['lecture_id'])) {
            $url_materials = " https://api.askphoenix.gr/api/material/".$_GET['lecture_id'];
            $materials_info = $asx_class->asx_general_curl($url_materials, $data_string = '', $args, '');
//            var_dump($materials_info);
            if ($materials_info) {
                //$data['materials_results'] = $materials_info['results'];
            }
        }

        $display_selected = array(
            'course_id'     => $this->session->data['course_id'],
            'course_name'   => $this->session->data['course_name'],
            'lecture_id'    => $this->session->data['lecture_id'],
            //'date_id'     => $this->session->data['date_id'],
            'date_format'   => $this->session->data['date_format'],
            ////exam info - Course info
            'exam_classroom_id'             => (!empty($this->session->data['class_room_id'])) ? $this->session->data['class_room_id'] : '',
            'exam_classroom_name'           => (!empty($this->session->data['class_room_name'])) ? $this->session->data['class_room_name'] : '',
            'exam_lecture_start_date_time'  => $this->session->data['lecture_start_date_time'],
            'exam_lecture_end_date_time'    => $this->session->data['lecture_end_date_time'],
            'exam_name' => (!empty($exam_name)) ? $exam_name : '',
            'exam_id'   => (!empty($exam_id)) ? $exam_id : '',
//            'exam_name' => (!empty($this->session->data[$this->request->get['lecture_id']]['exam_name'])) ? $this->session->data[$this->session->data['lecture_id']]['exam_name'] : '',
//            'exam_id'   => (!empty($this->session->data[$this->request->get['lecture_id']]['exam_id'])) ? $this->session->data[$this->session->data['lecture_id']]['exam_id'] : '',
            //'materials' => (!empty($materials_arr)) ? $materials_arr : ''
        );

        $data['display_selected'] = $display_selected;

        $data['course_text'] = $this->language->get('course_text');
        $data['all_text'] = $this->language->get('all_text');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['options'][] = array(
            'title_option' => $data['course_text'],
            'menu_option'   =>  '#'
        );

        $data['header'] = $this->load->controller('common/header_askphoenix');
        $data['footer'] = $this->load->controller('common/footer_askphoenix');

        $this->response->setOutput($this->load->view('teacher/exams', $data));
    }
}
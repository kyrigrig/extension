<?php
class ControllerTeacherSchedule extends Controller {
	public function index()
	{
        $asx_class = new Askphoenix();
        $auth = $asx_class->asx_check_token($this->session->data['token']);
        if(!empty($auth['asx_redirect'])) {
            $this->response->redirect($this->url->link($auth['asx_redirect'], '', true));
        }

//        $url = "https://api.askphoenix.gr/api/school";
        $args = array(
            'token' => $this->session->data['token']
        );

        //initiate values
        $this->load->language('teacher/schedule');
        $data = array();
        $data['current_url'] = 'index.php?route=teacher/schedule';//$_SERVER["REQUEST_URI"]; //$_SERVER["SERVER_NAME"]

        if(!empty($_GET['id'])) {
            $url = "https://api.askphoenix.gr/api/course/".$_GET['id']."/schedule/";
            $courses = $asx_class->asx_general_curl($url, $data_string = '', $args, '');
//            var_dump($courses);
            if($courses) {
                $courses_results_arr = array();
                foreach ($courses as $course) {
                    foreach ($course as $course_item) {
                        $courses_results_arr[] = array(
                        'id'            =>  $course_item->id,
                        'day_of_week'   =>  $course_item->dayOfWeek,
                        'start_time'    =>  date('H:i', strtotime($course_item->startTime)),
                        'end_time'      =>  date('H:i', strtotime($course_item->endTime)),
                        'classroom_id'  =>  $course_item->classroom->id,
                        'classroom_name'=>  $course_item->classroom->name
                        );
                    } //end foreach loop
                } //end foreach loop
//                var_dump($courses_results_arr);
//                die;
//                $week_schedule = array();
                $count = 0;
                foreach ($courses_results_arr as $course) {
                    if($course['day_of_week'] == 1) {
                        $week_schedule[1]['classroom_id'][] = $course['classroom_id'];
                        $week_schedule[1]['classroom_name'][] = $course['classroom_name'];
                        $week_schedule[1]['day_of_week'][] = $course['day_of_week'];
                        $week_schedule[1]['start_time'][] = $course['start_time'];
                        $week_schedule[1]['end_time'][] = $course['end_time'];
                        $count++;
                    }
                    if($course['day_of_week'] == 2) {
                        $week_schedule[2]['id'][] = $course['id'];
                        $week_schedule[2]['classroom_id'][] = $course['classroom_id'];
                        $week_schedule[2]['classroom_name'][] = $course['classroom_name'];
                        $week_schedule[2]['day_of_week'][] = $course['day_of_week'];
                        $week_schedule[2]['start_time'][] = $course['start_time'];
                        $week_schedule[2]['end_time'][] = $course['end_time'];
                        $count++;
                    }
                    if($course['day_of_week'] == 3) {
                        $week_schedule[3]['classroom_id'][] = $course['classroom_id'];
                        $week_schedule[3]['classroom_name'][] = $course['classroom_name'];
                        $week_schedule[3]['day_of_week'][] = $course['day_of_week'];
                        $week_schedule[3]['start_time'][] = $course['start_time'];
                        $week_schedule[3]['end_time'][] = $course['end_time'];
                        $count++;
                    }
                    if($course['day_of_week'] == 4) {
                        $week_schedule[4]['classroom_id'][] = $course['classroom_id'];
                        $week_schedule[4]['classroom_name'][] = $course['classroom_name'];
                        $week_schedule[4]['day_of_week'][] = $course['day_of_week'];
                        $week_schedule[4]['start_time'][] = $course['start_time'];
                        $week_schedule[4]['end_time'][] = $course['end_time'];
                        $count++;
                    }
                    elseif($course['day_of_week'] == 5) {
                        $week_schedule[5]['classroom_id'][] = $course['classroom_id'];
                        $week_schedule[5]['classroom_name'][] = $course['classroom_name'];
                        $week_schedule[5]['day_of_week'][] = $course['day_of_week'];
                        $week_schedule[5]['start_time'][] = $course['start_time'];
                        $week_schedule[5]['end_time'][] = $course['end_time'];
                    }
                }

//                var_dump($week_schedule[2]);
                $data['course_results'] = $courses_results_arr;
                $data['course_results_test'] = $week_schedule;
            } //end if
        } //end if $_GET

        $url_menu_courses = "https://api.askphoenix.gr/api/course";
        $courses_modal = $asx_class->asx_general_curl($url_menu_courses, $data_string = '', $args, '');


        $data['asx_results'] = $courses_modal['results'];

		$data['course_text'] = $this->language->get('course_text');
		$data['all_text'] = $this->language->get('all_text');
		
		$this->document->setTitle($this->language->get('heading_title'));
			
		$data['options'][] = array(
			'title_option' => $data['course_text'],
			'menu_option'   =>  '#'
		);
				
		$data['header'] = $this->load->controller('common/header_askphoenix');
		$data['footer'] = $this->load->controller('common/footer_askphoenix');
		
		$this->response->setOutput($this->load->view('teacher/schedule', $data));
	}
}
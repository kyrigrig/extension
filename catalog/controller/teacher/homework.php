<?php
class ControllerTeacherHomework extends Controller
{
    public function index()
    {
        $asx_class = new Askphoenix();
        $auth = $asx_class->asx_check_token($this->session->data['token']);
        if (!empty($auth['asx_redirect'])) {
            $this->response->redirect($this->url->link($auth['asx_redirect'], '', true));
        }

        $args = array(
            'token' => $this->session->data['token']
        );

        //initiate values
        $this->load->language('teacher/homework');
        $data = array();
        $data['current_url'] = 'index.php?route=teacher/homework';

        if(empty($_GET['id'])) { //reset all Exercise sessions if id does not exist
            //$asx_class->unsetSessionsExercises();
            unset($this->session->data['homework_course_id']);
            unset($this->session->data['homework_course_name']);
            unset($this->session->data['homework_date_id']);
            unset($this->session->data['homework_lecture_id']);
            unset($this->session->data['homework_date_format']);
            unset($this->session->data['homework_classroom_id']);
            unset($this->session->data['homework_classroom_name']);
            unset($this->session->data['homework_lecture_start_date_time']);
            unset($this->session->data['homework_lecture_end_date_time']);
        }
        else if(empty($_GET['lecture_id'])) {
            unset($this->session->data['homework_date_id']);
            unset($this->session->data['homework_lecture_id']);
            unset($this->session->data['homework_date_format']);
            unset($this->session->data['homework_classroom_id']);
            unset($this->session->data['homework_classroom_name']);
            unset($this->session->data['homework_lecture_start_date_time']);
            unset($this->session->data['homework_lecture_end_date_time']);
        }

        if(!empty($_GET['id'])) {
            $this->session->data['homework_course_id'] = $_GET['id'];
        }
        if(!empty($_GET['course_name'])) {
            $this->session->data['homework_course_name'] = $_GET['course_name'];
        }
        if(!empty($_GET['date_id'])) {
            $this->session->data['homework_date_id'] = $_GET['date_id'];
        }
        if(!empty($_GET['homework_lecture_id'])) { //pick a date
            $this->session->data['homework_lecture_id'] = $_GET['homework_lecture_id'];
        }
        if(!empty($_GET['date_format'])) { //date from pick a date
            $this->session->data['homework_date_format'] = $_GET['date_format'];
        }
        //END initiate values

        //POST CALLS
        if($this->request->post['submit_form'] == 1) { //POST Add New Exercise
            $url_add_exam = "https://api.askphoenix.gr/api/exercise/";
            $data_exam = array();
            $data_exam["comments"] = $this->request->post['homework_comments'];
            $data_exam["id"] = null;
            $data_exam["name"] = $this->request->post['homework_name'];
            $data_exam["page"] = $this->request->post['homework_page'];
            $data_exam["book"]["id"] = $this->request->post['book_id'];
            $data_exam["book"]["name"] = null; //$this->request->post['book_name']; //send is as null
            $data_exam["lecture"]["id"] = (!empty($this->session->data['homework_lecture_id'])) ? $this->session->data['homework_lecture_id']  : '';
            $data_exam["lecture"]["startDateTime"] =(!empty($this->session->data['homework_lecture_start_date_time'])) ? $this->session->data['homework_lecture_start_date_time'] : '';
            $data_exam["lecture"]["endDateTime"] = (!empty($this->session->data['homework_lecture_end_date_time'])) ? $this->session->data['homework_lecture_end_date_time'] : '';
            $data_exam["lecture"]["status"] = (!empty($this->session->data['homework_lecture_status'])) ? $this->session->data['homework_lecture_status'] : '';
            $data_exam["lecture"]["createdBy"] = 0;
            $data_exam["lecture"]["exercises"] = [];
            $data_exam["lecture"]["classroom"]["id"] = (!empty($this->session->data['homework_classroom_id'])) ? $this->session->data['homework_classroom_id'] : '';
            //$data_exam["lecture"]["classroom"]["name"] = (!empty($this->session->data['class_room_name'])) ? $this->session->data['class_room_name'] : '';
            $data_exam["lecture"]["course"]["firstDate"] = (!empty($this->session->data['homework_course_first_date'])) ?$this->session->data['homework_course_first_date'] : '';
            $data_exam["lecture"]["course"]["group"] = (!empty($this->session->data['homework_course_group'])) ? $this->session->data['homework_course_group'] : '';
            $data_exam["lecture"]["course"]["id"] = (!empty($this->session->data['homework_course_id'])) ? $this->session->data['homework_course_id'] : '';
            $data_exam["lecture"]["course"]["lastDate"] = (!empty($this->session->data['homework_course_last_date'])) ?$this->session->data['homework_course_last_date'] : '';
            $data_exam["lecture"]["course"]["level"] = (!empty($this->session->data['homework_course_level'])) ? $this->session->data['homework_course_level'] : '';
            $data_exam["lecture"]["course"]["name"] = (!empty($this->session->data['homework_course_name'])) ? $this->session->data['homework_course_name'] : '';
            $data_exam["lecture"]["course"]["subCourse"] = (!empty($this->session->data['homework_course_sub_course'])) ? $this->session->data['homework_course_sub_course'] : '';
            $data_string = json_encode($data_exam);
            $lectures_exercise = $asx_class->asx_curl_post($url_add_exam, $data_string, $args, '');
        } //end submit Exercise

        $url_menu_homework = "https://api.askphoenix.gr/api/course";
        $homework_modal = $asx_class->asx_general_curl($url_menu_homework, $data_string = '', $args, '');
        $data['asx_results'] = $homework_modal['results'];

        if(!empty($_GET['id']) || !empty($this->session->data['homework_course_id'])) { //get all lecture Dates
            $course_id = $_GET['id'] = $this->session->data['homework_course_id'];
            $data['pick_a_day'] = $this->language->get('pick_day_text');
            $url_menu_exams = "https://api.askphoenix.gr/api/course/".$course_id."/lecture/";
            $lectures_modal = $asx_class->asx_general_curl($url_menu_exams, $data_string = '', $args, '');
            //var_dump($lectures_modal);
            if($lectures_modal) {
                $data['asx_lectures_results'] = $lectures_modal['results'];
            } //end if lecture modal
        } //end if $_GET not empty course

        if(!empty($_GET['homework_lecture_id'])) { //Get Info from exercise
            $lecture_id = $_GET['homework_lecture_id'];
            $data['no_homework_title'] = $this->language->get('no_homework_title');
            $data['homework_title'] = $this->language->get('homework_title');

            $url_lectures_exams = "https://api.askphoenix.gr/api/lecture/".$lecture_id."/exercise";
            $lectures_homework = $asx_class->asx_general_curl($url_lectures_exams, $data_string = '', $args, '');

            if (!empty($lectures_homework['results'])) {
                $this->session->data[$this->request->get['homework_lecture_id']]['homework_exam_name'] = $lectures_homework['results'][0]->name;
                $this->session->data[$this->request->get['homework_lecture_id']]['homework_exam_id'] = $lectures_homework['results'][0]->id;
                $data['asx_lectures_homework_results'] = $lectures_homework['results'];
            } else {
                $data['asx_lectures_homework_results'] = $data['no_homework_title'];
            }
        } //end if $_GET not empty homework_lecture_id

        if(!empty($_GET['homework_lecture_id'])) { //GET Study Books if homework_lecture_id exists
            $url_books = "https://api.askphoenix.gr/api/course/" . $this->session->data['homework_course_id'] . "/book";
            $study_books = $asx_class->asx_general_curl($url_books, $data_string = '', $args, '');
            if ($study_books) {
                foreach ($study_books['results'] as $book_item) {
                    $study_books['exam_books'][] = array(
                        'exercise_study_books_id' => $book_item->id,
                        'exercise_study_books_name' => $book_item->name,
                    );
                } //end foreach loop
                $data['exercise_study_books'] = $study_books['exam_books'];
            }
        }

        if(!empty($_GET['date_format'])) { //get Course Data, when pick a Date
            $url_lecture_info = "https://api.askphoenix.gr/api/lecture/".$this->session->data['homework_lecture_id'];
            $lectures_info = $asx_class->asx_general_curl($url_lecture_info, $data_string = '', $args, '');
            if($lectures_info) {
                $data['asx_lectures_info_results'] = $lectures_info['results'];
                ////set values to sessions
                //classroom info
                $this->session->data['homework_classroom_id'] = $data['asx_lectures_info_results']->classroom->id;
                $this->session->data['homework_classroom_name'] = $data['asx_lectures_info_results']->classroom->name;
                //course Info
                $this->session->data['homework_course_id'] = $data['asx_lectures_info_results']->course->id;
                $this->session->data['homework_course_name'] = (!empty($this->session->data['homework_course_name'])) ? $this->session->data['homework_course_name'] : $data['asx_lectures_info_results']->course->name;
                $this->session->data['homework_course_sub_course'] = $data['asx_lectures_info_results']->course->subCourse;
                $this->session->data['homework_course_level'] = $data['asx_lectures_info_results']->course->level;
                $this->session->data['homework_course_group'] = $data['asx_lectures_info_results']->course->group;
                $this->session->data['homework_course_first_date'] = $data['asx_lectures_info_results']->course->firstDate;
                $this->session->data['homework_course_last_date'] = $data['asx_lectures_info_results']->course->lastDate;
                //general
                $this->session->data['homework_lecture_start_date_time'] = $data['asx_lectures_info_results']->startDateTime;
                $this->session->data['homework_lecture_end_date_time'] = $data['asx_lectures_info_results']->endDateTime;
                $this->session->data['homework_lecture_status'] = $data['asx_lectures_info_results']->status;
                $this->session->data['homework_lecture_created_by'] = $data['asx_lectures_info_results']->createdBy;
            }
        }

        ////before Display, Call Edit Exercise API

        ////display Exercises if exists
        if(!empty($_GET['homework_lecture_id'])) {
            $url_exercise = "https://api.askphoenix.gr/api/lecture/" . $lecture_id . "/exercise";
            $exercises_info = $asx_class->asx_general_curl($url_exercise, $data_string = '', $args, '');
            if ($exercises_info) {
                $data['exercises_results'] = $exercises_info['results'];
            }
        }


        $display_selected = array(
            'course_id'     => $this->session->data['homework_course_id'],
            'course_name'   => $this->session->data['homework_course_name'],
            'lecture_id'    => $this->session->data['homework_lecture_id'],
            //'date_id'     => $this->session->data['date_id'],
            'date_format'   => $this->session->data['homework_date_format'],
            ////exam info - Course info
            'homework_classroom_name'           => $this->session->data['homework_classroom_name'],
            'homework_lecture_start_date_time'  => $this->session->data['homework_lecture_start_date_time'],
            'homework_lecture_end_date_time'    => $this->session->data['homework_lecture_end_date_time'],
            'homework_classroom_id'     => (!empty($this->session->data['homework_classroom_id'])) ? $this->session->data['homework_classroom_id'] : '',
//            'homework_exam_name'        => (!empty($this->session->data[$this->request->get['homework_lecture_id']]['homework_exam_name'])) ? $this->session->data[$this->session->data['homework_lecture_id']]['homework_exam_name'] : '',
//            'homework_exam_id'          => (!empty($this->session->data[$this->request->get['homework_lecture_id']]['homework_exam_id'])) ? $this->session->data[$this->session->data['homework_lecture_id']]['homework_exam_id'] : ''
        );

        $data['display_selected'] = $display_selected;


        $data['course_text'] = $this->language->get('course_text');
        $data['all_text'] = $this->language->get('all_text');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['options'][] = array(
            'title_option' => $data['course_text'],
            'menu_option'   =>  '#'
        );

        $data['header'] = $this->load->controller('common/header_askphoenix');
        $data['footer'] = $this->load->controller('common/footer_askphoenix');

        $this->response->setOutput($this->load->view('teacher/homework', $data));
    }

}
<?php
class ControllerTeacherMore extends Controller {
	public function index()
	{
        $asx_class = new Askphoenix();
        $auth = $asx_class->asx_check_token($this->session->data['token']);
        if(!empty($auth['asx_redirect'])) {
            $this->response->redirect($this->url->link($auth['asx_redirect'], '', true));
        }

		$this->load->language('teacher/more');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$data['options'] = array();
		
		$data['options'][] = array(
			'title_option' => 'Account',
			'menu_option'   =>  $this->url->link('teacher/account')
		);
		
		$data['options'][] = array(
			'title_option' => 'Language',
			'menu_option'   =>  $this->url->link('teacher/language')
		);
		
//		$data['options'][] = array(
//			'title_option' => 'Help',
//			'menu_option'   =>  $this->url->link('teacher/help')
//		);
		
		$data['options'][] = array(
			'title_option' => 'Terms of use',
			'menu_option'   =>  '#terms-of-use'
		);
		
		$data['options'][] = array(
			'title_option' => 'Privacy Policy',
			'menu_option'   =>  'https://www.askphoenix.gr/privacy-policy/'
		);
		
		$data['options'][] = array(
			'title_option' => 'Logout',
			'menu_option'   =>  $this->url->link('account/logout')
		);
		
		$data['header'] = $this->load->controller('common/header_askphoenix');
		$data['footer'] = $this->load->controller('common/footer_askphoenix');
		
		$this->response->setOutput($this->load->view('teacher/more', $data));
	}
}
<?php
class ControllerAccountLogin extends Controller {
	public function index() {
		$this->load->language('account/login');

        if(empty($this->session->data['token'])) {
            if (!empty($_POST['username']) && !empty($_POST['password'])) {
                //Curl Auth User
                $url = "https://api.askphoenix.gr/api/authentication/authenticate/basic";
                $data = array(
                    'username' => $_POST['username'],
                    'password' => $_POST['password']
                );
                $data_string = json_encode($data);
                $asx_class = new Askphoenix();
                $login_user = $asx_class->asx_curl_login_user($url, $data_string, 'schedule');
                    //var_dump($login_user);
                if ($login_user['asx_http_code'] == 200 && !empty($login_user['asx_token'])) { //get token
                    $this->session->data['token'] = $login_user['asx_token'];
                    if(!empty($login_user['asx_redirect'])) {
                        $this->response->redirect($this->url->link('teacher/'.$login_user['asx_redirect']));
                    }
                } elseif ($login_user['asx_code'] == 1 || !empty($login_user['asx_message'])) {
                    $data['code'] = $login_user['asx_code'] ;
                    $data['message'] = $login_user['asx_message'] ;
                }
            }
        }
        elseif(isset($this->session->data['token']) && !empty($this->session->data['token'])) {
            $this->response->redirect($this->url->link('teacher/schedule'));
        }

		$data['header_askphoenix'] = $this->load->controller('common/header_askphoenix');
		$data['footer_askphoenix'] = $this->load->controller('common/footer_askphoenix');
		
		$this->response->setOutput($this->load->view('account/login', $data));
	}
}
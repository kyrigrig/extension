<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/teacher/exams.twig */
class __TwigTemplate_7492a62478a515d01ab3ab8e446d992ad6cf7003457701af8e90f849f0e56326 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div class=\"container-fluid asx-header-container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <h1><i class=\"fa fa-calendar\" style=\"margin-right:20px;\"></i>";
        // line 5
        echo ($context["heading_title"] ?? null);
        echo "</h1>
        </div>
    </div>
</div>
";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["options"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
            // line 10
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-exam-courses\" data-toggle=\"modal\" data-target=\"#myExamCourses\">
                <a href=\"";
            // line 13
            echo twig_get_attribute($this->env, $this->source, $context["option"], "menu_option", [], "any", false, false, false, 13);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["option"], "title_option", [], "any", false, false, false, 13);
            echo "</a>
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-exam-courses text-right\" data-toggle=\"modal\" data-target=\"#myExamCourses\">
                <a href=\"";
            // line 16
            echo twig_get_attribute($this->env, $this->source, $context["option"], "menu_option", [], "any", false, false, false, 16);
            echo "\">
                    ";
            // line 17
            if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_id", [], "any", false, false, false, 17)) {
                // line 18
                echo "                        ";
                echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 18);
                echo "
                    ";
            } else {
                // line 20
                echo "                        ";
                echo ($context["choice_text"] ?? null);
                echo "
                    ";
            }
            // line 22
            echo "                    &nbsp;&nbsp;<i class=\"fa fa-arrow-down\"></i>
                </a>
            </div>
        </div>
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "
";
        // line 29
        if (($context["pick_a_day"] ?? null)) {
            // line 30
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-pick-a-day\" data-toggle=\"modal\" data-target=\"#myExamLectures\">
                ";
            // line 33
            echo ($context["pick_a_day"] ?? null);
            echo "
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-pick-a-day text-right\">
                ";
            // line 36
            if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 36)) {
                // line 37
                echo "                    ";
                echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 37);
                echo "
                ";
            } else {
                // line 39
                echo "                    ";
                echo ($context["choice_text"] ?? null);
                echo "
                ";
            }
            // line 41
            echo "                <i class=\"fa fa-arrow-down\"></i>
            </div>
        </div>
    </div>
";
        }
        // line 46
        echo "
";
        // line 47
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_classroom_name", [], "any", false, false, false, 47)) {
            // line 48
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-pick-a-day\">";
            // line 50
            echo ($context["classroom_title"] ?? null);
            echo " ";
            echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_classroom_name", [], "any", false, false, false, 50);
            echo "</div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-classroom-name text-right\">";
            // line 51
            echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_lecture_start_date_time", [], "any", false, false, false, 51), "G:ia", "Europe/Athens");
            echo " - ";
            echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_lecture_end_date_time", [], "any", false, false, false, 51), "G:ia", "Europe/Athens");
            echo "</div>
        </div>
    </div>
";
        }
        // line 55
        echo "
";
        // line 56
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 56)) {
            // line 57
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12 asx-exam text-center\">
                <strong>";
            // line 60
            echo ($context["heading_add_exam_title"] ?? null);
            echo "</strong>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-exam\" data-toggle=\"modal\" data-target=\"#myExamEdit\">
                ";
            // line 65
            echo ($context["heading_add_exam_title"] ?? null);
            echo ": ";
            echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 65);
            echo "
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-exam text-right\" data-toggle=\"modal\" data-target=\"#myExamEdit\">
                ";
            // line 68
            echo ($context["edit_exam_btn_title"] ?? null);
            echo "&nbsp;<i class=\"fa fa-arrow-right\" title=\"";
            echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 68);
            echo "\"></i>
            </div>
        </div>
    </div>
";
        } else {
            // line 73
            echo "        <div class=\"container-fluid\">
            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12 col-xs-12 text-center\">
                    <p class=\"asx-mt-10\">";
            // line 76
            echo ($context["asx_lectures_exams_results"] ?? null);
            echo "</p>
                </div>
            </div>
        </div>
";
        }
        // line 81
        echo "
";
        // line 82
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "materials", [], "any", false, false, false, 82)) {
            // line 83
            echo "    <div class=\"container-fluid\">
";
            // line 89
            echo "        <div class=\"row\">
            ";
            // line 90
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "materials", [], "any", false, false, false, 90));
            foreach ($context['_seq'] as $context["_key"] => $context["material_item"]) {
                // line 91
                echo "            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-exam\" data-toggle=\"modal\" data-target=\"#myExamMaterialEdit\">
                ";
                // line 92
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_book_name", [], "any", false, false, false, 92);
                echo "
            </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['material_item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 95
            echo "            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-exam text-right\" data-toggle=\"modal\" data-target=\"#myExamMaterialEdit\">
                ";
            // line 96
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "materials", [], "any", false, false, false, 96), "material_chapter", [], "any", false, false, false, 96);
            echo ", ";
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "materials", [], "any", false, false, false, 96), "material_section", [], "any", false, false, false, 96);
            echo ", ";
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "materials", [], "any", false, false, false, 96), "material_comments", [], "any", false, false, false, 96);
            echo "
                <i class=\"fa fa-arrow-right\" title=\"";
            // line 97
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "materials", [], "any", false, false, false, 97), "material_chapter", [], "any", false, false, false, 97);
            echo ", ";
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "materials", [], "any", false, false, false, 97), "material_section", [], "any", false, false, false, 97);
            echo ", ";
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "materials", [], "any", false, false, false, 97), "material_comments", [], "any", false, false, false, 97);
            echo "\"></i>
            </div>
        </div>
    </div>
";
        }
        // line 110
        echo "
";
        // line 111
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "lecture_id", [], "any", false, false, false, 111)) {
            // line 112
            echo "    <div class=\"menu\">
        <div class=\"btn trigger\">
            <span class=\"line\"></span>
        </div>
        <div class=\"icons\">
            ";
            // line 117
            if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 117)) {
                // line 118
                echo "                <div class=\"rotater\" data-toggle=\"modal\" data-target=\"#myAddExamMaterial\">
                    <div class=\"btn btn-icon\">
                        <i class=\"fa fa-pencil\" title=\"";
                // line 120
                echo ($context["add_exam_ili_title"] ?? null);
                echo "\"></i>
                    </div>
                </div>
            ";
            } else {
                // line 124
                echo "                <div class=\"rotater\" data-toggle=\"modal\" data-target=\"#myAddExam\">
                    <div class=\"btn btn-icon\">
                        <i class=\"fa fa-file\" title=\"";
                // line 126
                echo ($context["add_exam_title"] ?? null);
                echo "\"></i>
                    </div>
                </div>
            ";
            }
            // line 130
            echo "        </div>
    </div>
";
        }
        // line 133
        echo "
";
        // line 135
        echo "
<div class=\"modal fade\" id=\"myExamCourses\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">";
        // line 141
        echo ($context["modal_courses"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                ";
        // line 144
        if (($context["asx_results"] ?? null)) {
            // line 145
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["asx_results"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["result"]) {
                // line 146
                echo "                        <a href=\"";
                echo ($context["current_url"] ?? null);
                echo "&id=";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "id", [], "any", false, false, false, 146);
                echo "&course_name=";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "name", [], "any", false, false, false, 146);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "subCourse", [], "any", false, false, false, 146);
                echo " - ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "level", [], "any", false, false, false, 146);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "name", [], "any", false, false, false, 146);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "subCourse", [], "any", false, false, false, 146);
                echo " - ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "level", [], "any", false, false, false, 146);
                echo "</a><br/><br/>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 148
            echo "                ";
        }
        // line 149
        echo "            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 151
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myExamLectures\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModaLectureslLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModaLectureslLabel\">";
        // line 162
        echo ($context["pick_day_text"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                ";
        // line 165
        if (($context["asx_lectures_results"] ?? null)) {
            // line 166
            echo "                    <ul>
                        ";
            // line 167
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["asx_lectures_results"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["result_lecture"]) {
                // line 168
                echo "                            <li>
                                <a href=\"";
                // line 169
                echo ($context["current_url"] ?? null);
                echo "&id=";
                echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_id", [], "any", false, false, false, 169);
                echo "&lecture_id=";
                echo twig_get_attribute($this->env, $this->source, $context["result_lecture"], "id", [], "any", false, false, false, 169);
                echo "&date_format=";
                echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["result_lecture"], "startDateTime", [], "any", false, false, false, 169), "d/m/Y, D G:ia", "Europe/Athens");
                echo "\">";
                echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["result_lecture"], "startDateTime", [], "any", false, false, false, 169), "d/m/Y, D G:ia", "Europe/Athens");
                echo " (";
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["result_lecture"], "exam", [], "any", false, false, false, 169), "name", [], "any", false, false, false, 169);
                echo ")</a><br/><br/>
                            </li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result_lecture'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 172
            echo "                    </ul>
                ";
        }
        // line 174
        echo "            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 176
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myAddExamMaterial\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myAddExamMaterialLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myAddExamMaterialLabel\">";
        // line 187
        echo ($context["heading_add_study_title"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <form action=\"\" method=\"post\" role=\"form\" class=\"asx-form-books\">
                    <div class=\"modal-body\">
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 193
        echo ($context["course_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 194
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 194);
        echo "</div>
                            <input type=\"hidden\" name=\"course_name\" value=\"";
        // line 195
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 195);
        echo "\" />
                            <input type=\"hidden\" name=\"exam_id\" value=\"";
        // line 196
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_id", [], "any", false, false, false, 196);
        echo "\" />
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 199
        echo ($context["pick_day_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 200
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 200);
        echo "</div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 203
        echo ($context["modal_add_study_book_title"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">
                                <select name=\"book\" id=\"book\">
                                    ";
        // line 206
        if (($context["exam_study_books"] ?? null)) {
            // line 207
            echo "                                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["exam_study_books"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["exam_book_item"]) {
                // line 208
                echo "                                            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["exam_book_item"], "exam_study_books_id", [], "any", false, false, false, 208);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["exam_book_item"], "exam_study_books_name", [], "any", false, false, false, 208);
                echo "</option>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['exam_book_item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 210
            echo "                                    ";
        }
        // line 211
        echo "                                </select>
                            </div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 215
        echo ($context["modal_add_study_chapter_exam_name"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"chapter\" id=\"chapter\" /></div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 219
        echo ($context["modal_add_study_section_name"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"section\" id=\"section\" /></div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 223
        echo ($context["modal_add_study_comments_title"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"comments\" id=\"comments\" /></div>
                        </div>

                        <div class=\"col-md-8\"><input type=\"hidden\" name=\"submit_material_form\" id=\"submit_material_form\" value=\"1\" /></div>
                    </div>
                    <div class=\"modal-footer\">
                        <button class=\"btn btn-secondary asx-btn-modal\">";
        // line 230
        echo ($context["add_material_btn_title"] ?? null);
        echo " <i class=\"fa fa-paper-plane-o ml-1\"></i></button>
                    </div>
                </form>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 235
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myAddExam\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myAddExamLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myAddExamLabel\">";
        // line 246
        echo ($context["heading_add_exam_title"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <form action=\"\" method=\"post\" role=\"form\" class=\"asx-form-books\">
                    <div class=\"modal-body\">
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 252
        echo ($context["course_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 253
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 253);
        echo "</div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 256
        echo ($context["pick_day_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 257
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 257);
        echo "</div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 260
        echo ($context["modal_add_exam_name"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"exam_name\" id=\"exam_name\" /></div>
                        </div>
                        <div class=\"col-md-8\"><input type=\"hidden\" name=\"submit_form\" id=\"submit_form\" value=\"1\" /></div>
                    </div>
                    <div class=\"modal-footer\">
                        <button class=\"btn btn-secondary asx-btn-modal\">";
        // line 266
        echo ($context["add_exam_btn_title"] ?? null);
        echo " <i class=\"fa fa-paper-plane-o ml-1\"></i></button>
                    </div>
                </form>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 271
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myExamEdit\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myExamEdit\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myExamEdit\">";
        // line 282
        echo ($context["heading_add_exam_title"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <div class=\"form-group row\">
                    <div class=\"col-md-1 col-sm-1 col-xs-1\"><i class=\"fa fa-trash fa-2x\"></i></div>
                    <div class=\"col-md-11 col-sm-11 col-xs-11\" style=\"line-height: 32px;\">
                        <a href=\"";
        // line 288
        echo ($context["current_url"] ?? null);
        echo "&delete=";
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_id", [], "any", false, false, false, 288);
        echo "\">";
        echo ($context["edit_exam_delete_title"] ?? null);
        echo "</a>
                    </div>
                </div>
";
        // line 295
        echo "                <div class=\"form-group row\">
                    <div class=\"col-md-1 col-sm-1 col-xs-1\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><i class=\"fa fa-close fa-2x\"></i></div>
                    <div class=\"col-md-11 col-sm-11 col-xs-11\" data-dismiss=\"modal\" aria-label=\"Close\" style=\"line-height: 32px;\">";
        // line 297
        echo ($context["edit_exam_cancel_title"] ?? null);
        echo "</div>
                </div>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 301
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>



<script>
    \$(document).ready(function() {
        \$(\".trigger\").click(function() {
            \$(\".menu\").toggleClass(\"active\");
        });
    });
</script>

";
        // line 317
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/teacher/exams.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  650 => 317,  631 => 301,  624 => 297,  620 => 295,  610 => 288,  601 => 282,  587 => 271,  579 => 266,  570 => 260,  564 => 257,  560 => 256,  554 => 253,  550 => 252,  541 => 246,  527 => 235,  519 => 230,  509 => 223,  502 => 219,  495 => 215,  489 => 211,  486 => 210,  475 => 208,  470 => 207,  468 => 206,  462 => 203,  456 => 200,  452 => 199,  446 => 196,  442 => 195,  438 => 194,  434 => 193,  425 => 187,  411 => 176,  407 => 174,  403 => 172,  384 => 169,  381 => 168,  377 => 167,  374 => 166,  372 => 165,  366 => 162,  352 => 151,  348 => 149,  345 => 148,  322 => 146,  317 => 145,  315 => 144,  309 => 141,  301 => 135,  298 => 133,  293 => 130,  286 => 126,  282 => 124,  275 => 120,  271 => 118,  269 => 117,  262 => 112,  260 => 111,  257 => 110,  245 => 97,  237 => 96,  234 => 95,  225 => 92,  222 => 91,  218 => 90,  215 => 89,  212 => 83,  210 => 82,  207 => 81,  199 => 76,  194 => 73,  184 => 68,  176 => 65,  168 => 60,  163 => 57,  161 => 56,  158 => 55,  149 => 51,  143 => 50,  139 => 48,  137 => 47,  134 => 46,  127 => 41,  121 => 39,  115 => 37,  113 => 36,  107 => 33,  102 => 30,  100 => 29,  97 => 28,  86 => 22,  80 => 20,  74 => 18,  72 => 17,  68 => 16,  60 => 13,  55 => 10,  51 => 9,  44 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/teacher/exams.twig", "");
    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/teacher/exams.twig */
class __TwigTemplate_eaf2fdc89358be94dfd7bc1599a7dc4601cc232ed50548d8f170184f86ce4179 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div class=\"container-fluid asx-header-container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <h1><i class=\"fa fa-calendar\" style=\"margin-right:20px;\"></i>";
        // line 5
        echo ($context["heading_title"] ?? null);
        echo "</h1>
        </div>
    </div>
</div>
";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["options"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
            // line 10
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-exam-courses\" data-toggle=\"modal\" data-target=\"#myExamCourses\">
                <a href=\"";
            // line 13
            echo twig_get_attribute($this->env, $this->source, $context["option"], "menu_option", [], "any", false, false, false, 13);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["option"], "title_option", [], "any", false, false, false, 13);
            echo "</a>
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-exam-courses text-right\" data-toggle=\"modal\" data-target=\"#myExamCourses\">
                <a href=\"";
            // line 16
            echo twig_get_attribute($this->env, $this->source, $context["option"], "menu_option", [], "any", false, false, false, 16);
            echo "\">
                    ";
            // line 17
            if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_id", [], "any", false, false, false, 17)) {
                // line 18
                echo "                        ";
                echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 18);
                echo "
                    ";
            } else {
                // line 20
                echo "                        ";
                echo ($context["choice_text"] ?? null);
                echo "
                    ";
            }
            // line 22
            echo "                    &nbsp;&nbsp;<i class=\"fa fa-arrow-down\"></i>
                </a>
            </div>
        </div>
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "
";
        // line 29
        if (($context["pick_a_day"] ?? null)) {
            // line 30
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-pick-a-day\" data-toggle=\"modal\" data-target=\"#myExamLectures\">
                ";
            // line 33
            echo ($context["pick_a_day"] ?? null);
            echo "
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-pick-a-day text-right\">
                ";
            // line 36
            if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 36)) {
                // line 37
                echo "                    ";
                echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 37);
                echo "
                ";
            } else {
                // line 39
                echo "                    ";
                echo ($context["choice_text"] ?? null);
                echo "
                ";
            }
            // line 41
            echo "                <i class=\"fa fa-arrow-down\"></i>
            </div>
        </div>
    </div>
";
        }
        // line 46
        echo "
";
        // line 47
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_classroom_name", [], "any", false, false, false, 47)) {
            // line 48
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-pick-a-day\">";
            // line 50
            echo ($context["classroom_title"] ?? null);
            echo " ";
            echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_classroom_name", [], "any", false, false, false, 50);
            echo "</div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-classroom-name text-right\">";
            // line 51
            echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_lecture_start_date_time", [], "any", false, false, false, 51), "G:ia", "Europe/Athens");
            echo " - ";
            echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_lecture_end_date_time", [], "any", false, false, false, 51), "G:ia", "Europe/Athens");
            echo "</div>
        </div>
    </div>
";
        }
        // line 55
        echo "
";
        // line 56
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 56)) {
            // line 57
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12 asx-exam text-center\">
                <strong>";
            // line 60
            echo ($context["heading_add_exam_title"] ?? null);
            echo "</strong>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-exam\" data-toggle=\"modal\" data-target=\"#myExamEdit\">
                ";
            // line 65
            echo ($context["heading_add_exam_title"] ?? null);
            echo ": ";
            echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 65);
            echo "
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-exam text-right\" data-toggle=\"modal\" data-target=\"#myExamEdit\">
                ";
            // line 68
            echo ($context["edit_exam_btn_title"] ?? null);
            echo "&nbsp;<i class=\"fa fa-arrow-right\" title=\"";
            echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 68);
            echo "\"></i>
            </div>
        </div>
    </div>
";
        } else {
            // line 73
            echo "        <div class=\"container-fluid\">
            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12 col-xs-12 text-center\">
                    <p class=\"asx-mt-10\">";
            // line 76
            echo ($context["asx_lectures_exams_results"] ?? null);
            echo "</p>
                </div>
            </div>
        </div>
";
        }
        // line 81
        echo "
";
        // line 82
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "lecture_id", [], "any", false, false, false, 82)) {
            // line 83
            echo "    <div class=\"menu\">
        <div class=\"btn trigger\">
            <span class=\"line\"></span>
        </div>
        <div class=\"icons\">
            ";
            // line 88
            if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 88)) {
                // line 89
                echo "                <div class=\"rotater\" data-toggle=\"modal\" data-target=\"#myAddExamMaterial\">
                    <div class=\"btn btn-icon\">
                        <i class=\"fa fa-pencil\" title=\"";
                // line 91
                echo ($context["add_exam_ili_title"] ?? null);
                echo "\"></i>
                    </div>
                </div>
            ";
            } else {
                // line 95
                echo "                <div class=\"rotater\" data-toggle=\"modal\" data-target=\"#myAddExam\">
                    <div class=\"btn btn-icon\">
                        <i class=\"fa fa-file\" title=\"";
                // line 97
                echo ($context["add_exam_title"] ?? null);
                echo "\"></i>
                    </div>
                </div>
            ";
            }
            // line 101
            echo "        </div>
    </div>
";
        }
        // line 104
        echo "
";
        // line 106
        echo "
<div class=\"modal fade\" id=\"myExamCourses\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">";
        // line 112
        echo ($context["modal_courses"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                ";
        // line 115
        if (($context["asx_results"] ?? null)) {
            // line 116
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["asx_results"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["result"]) {
                // line 117
                echo "                        <a href=\"";
                echo ($context["current_url"] ?? null);
                echo "&id=";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "id", [], "any", false, false, false, 117);
                echo "&course_name=";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "name", [], "any", false, false, false, 117);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "subCourse", [], "any", false, false, false, 117);
                echo " - ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "level", [], "any", false, false, false, 117);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "name", [], "any", false, false, false, 117);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "subCourse", [], "any", false, false, false, 117);
                echo " - ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "level", [], "any", false, false, false, 117);
                echo "</a><br/><br/>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 119
            echo "                ";
        }
        // line 120
        echo "            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 122
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myExamLectures\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModaLectureslLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModaLectureslLabel\">";
        // line 133
        echo ($context["pick_day_text"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                ";
        // line 136
        if (($context["asx_lectures_results"] ?? null)) {
            // line 137
            echo "                    <ul>
                        ";
            // line 138
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["asx_lectures_results"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["result_lecture"]) {
                // line 139
                echo "                            <li>
                                <a href=\"";
                // line 140
                echo ($context["current_url"] ?? null);
                echo "&id=";
                echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_id", [], "any", false, false, false, 140);
                echo "&lecture_id=";
                echo twig_get_attribute($this->env, $this->source, $context["result_lecture"], "id", [], "any", false, false, false, 140);
                echo "&date_format=";
                echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["result_lecture"], "startDateTime", [], "any", false, false, false, 140), "d/m/Y, D G:ia", "Europe/Athens");
                echo "\">";
                echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["result_lecture"], "startDateTime", [], "any", false, false, false, 140), "d/m/Y, D G:ia", "Europe/Athens");
                echo " (";
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["result_lecture"], "exam", [], "any", false, false, false, 140), "name", [], "any", false, false, false, 140);
                echo ")</a><br/><br/>
                            </li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result_lecture'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 143
            echo "                    </ul>
                ";
        }
        // line 145
        echo "            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 147
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myAddExamMaterial\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myAddExamMaterialLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myAddExamMaterialLabel\">";
        // line 158
        echo ($context["heading_add_study_title"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <form action=\"\" method=\"post\" role=\"form\" class=\"asx-form-books\">
                    <div class=\"modal-body\">
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 164
        echo ($context["course_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 165
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 165);
        echo "</div>
                            <input type=\"hidden\" name=\"course_name\" value=\"";
        // line 166
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 166);
        echo "\" />
                            ";
        // line 167
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_id", [], "any", false, false, false, 167);
        echo "
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 170
        echo ($context["pick_day_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 171
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 171);
        echo "</div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 174
        echo ($context["modal_add_study_book_title"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">
                                <select name=\"book\" id=\"book\">
                                    ";
        // line 177
        if (($context["exam_study_books"] ?? null)) {
            // line 178
            echo "                                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["exam_study_books"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["exam_book_item"]) {
                // line 179
                echo "                                            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["exam_book_item"], "exam_study_books_id", [], "any", false, false, false, 179);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["exam_book_item"], "exam_study_books_name", [], "any", false, false, false, 179);
                echo "</option>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['exam_book_item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 181
            echo "                                    ";
        }
        // line 182
        echo "                                </select>
                            </div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 186
        echo ($context["modal_add_study_chapter_exam_name"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"chapter\" id=\"chapter\" /></div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 190
        echo ($context["modal_add_study_section_name"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"section\" id=\"section\" /></div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 194
        echo ($context["modal_add_study_comments_title"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"comments\" id=\"comments\" /></div>
                        </div>

                        <div class=\"col-md-8\"><input type=\"hidden\" name=\"submit_material_form\" id=\"submit_material_form\" value=\"1\" /></div>
                    </div>
                    <div class=\"modal-footer\">
                        <button class=\"btn btn-secondary asx-btn-modal\">";
        // line 201
        echo ($context["add_material_btn_title"] ?? null);
        echo " <i class=\"fa fa-paper-plane-o ml-1\"></i></button>
                    </div>
                </form>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 206
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myAddExam\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myAddExamLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myAddExamLabel\">";
        // line 217
        echo ($context["heading_add_exam_title"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <form action=\"\" method=\"post\" role=\"form\" class=\"asx-form-books\">
                    <div class=\"modal-body\">
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 223
        echo ($context["course_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 224
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 224);
        echo "</div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 227
        echo ($context["pick_day_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 228
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 228);
        echo "</div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 231
        echo ($context["modal_add_exam_name"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"exam_name\" id=\"exam_name\" /></div>
                        </div>
                        <div class=\"col-md-8\"><input type=\"hidden\" name=\"submit_form\" id=\"submit_form\" value=\"1\" /></div>
                    </div>
                    <div class=\"modal-footer\">
                        <button class=\"btn btn-secondary asx-btn-modal\">";
        // line 237
        echo ($context["add_exam_btn_title"] ?? null);
        echo " <i class=\"fa fa-paper-plane-o ml-1\"></i></button>
                    </div>
                </form>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 242
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myExamEdit\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myExamEdit\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myExamEdit\">";
        // line 253
        echo ($context["heading_add_exam_title"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <div class=\"form-group row\">
                    <div class=\"col-md-1 col-sm-1 col-xs-1\"><i class=\"fa fa-trash fa-2x\"></i></div>
                    <div class=\"col-md-11 col-sm-11 col-xs-11\" style=\"line-height: 32px;\">
                        <a href=\"";
        // line 259
        echo ($context["current_url"] ?? null);
        echo "&delete=";
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_id", [], "any", false, false, false, 259);
        echo "\">";
        echo ($context["edit_exam_delete_title"] ?? null);
        echo "</a>
                    </div>
                </div>
";
        // line 266
        echo "                <div class=\"form-group row\">
                    <div class=\"col-md-1 col-sm-1 col-xs-1\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><i class=\"fa fa-close fa-2x\"></i></div>
                    <div class=\"col-md-11 col-sm-11 col-xs-11\" data-dismiss=\"modal\" aria-label=\"Close\" style=\"line-height: 32px;\">";
        // line 268
        echo ($context["edit_exam_cancel_title"] ?? null);
        echo "</div>
                </div>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 272
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<script>
    \$(document).ready(function() {
        \$(\".trigger\").click(function() {
            \$(\".menu\").toggleClass(\"active\");
        });
    });
</script>

";
        // line 286
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/teacher/exams.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  598 => 286,  581 => 272,  574 => 268,  570 => 266,  560 => 259,  551 => 253,  537 => 242,  529 => 237,  520 => 231,  514 => 228,  510 => 227,  504 => 224,  500 => 223,  491 => 217,  477 => 206,  469 => 201,  459 => 194,  452 => 190,  445 => 186,  439 => 182,  436 => 181,  425 => 179,  420 => 178,  418 => 177,  412 => 174,  406 => 171,  402 => 170,  396 => 167,  392 => 166,  388 => 165,  384 => 164,  375 => 158,  361 => 147,  357 => 145,  353 => 143,  334 => 140,  331 => 139,  327 => 138,  324 => 137,  322 => 136,  316 => 133,  302 => 122,  298 => 120,  295 => 119,  272 => 117,  267 => 116,  265 => 115,  259 => 112,  251 => 106,  248 => 104,  243 => 101,  236 => 97,  232 => 95,  225 => 91,  221 => 89,  219 => 88,  212 => 83,  210 => 82,  207 => 81,  199 => 76,  194 => 73,  184 => 68,  176 => 65,  168 => 60,  163 => 57,  161 => 56,  158 => 55,  149 => 51,  143 => 50,  139 => 48,  137 => 47,  134 => 46,  127 => 41,  121 => 39,  115 => 37,  113 => 36,  107 => 33,  102 => 30,  100 => 29,  97 => 28,  86 => 22,  80 => 20,  74 => 18,  72 => 17,  68 => 16,  60 => 13,  55 => 10,  51 => 9,  44 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/teacher/exams.twig", "");
    }
}

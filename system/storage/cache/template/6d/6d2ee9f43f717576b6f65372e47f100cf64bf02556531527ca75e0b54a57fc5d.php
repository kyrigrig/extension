<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/common/footer_askphoenix.twig */
class __TwigTemplate_3e4dfcffed932fdb804f84d97ae5de6bb731e458beb480b8fa0e1b053290984a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["token"] ?? null)) {
            // line 2
            echo "    <footer>
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-3 col-sm-3 col-xs-3 text-center asx-footer\">
                    <a href=\"";
            // line 6
            echo ($context["homework_href"] ?? null);
            echo "\">
                        <i class=\"fa fa-book fa-2x\"></i><br/>
                        ";
            // line 8
            echo ($context["homework_menu"] ?? null);
            echo "</a>
                </div>
                <div class=\"col-md-3 col-sm-3 col-xs-3 text-center asx-footer\">
                    <a href=\"";
            // line 11
            echo ($context["exams_href"] ?? null);
            echo "\">
                        <i class=\"fa fa-file fa-2x\"></i><br/>
                        ";
            // line 13
            echo ($context["exams_menu"] ?? null);
            echo "</a>
                </div>
                <div class=\"col-md-3 col-sm-3 col-xs-3 text-center asx-footer\" data-toggle=\"modal\" data-target=\"#myScheduleTest\">
                    <a href=\"";
            // line 16
            echo ($context["schedule_href"] ?? null);
            echo "\">
                        <i class=\"fa fa-calendar fa-2x\"></i><br/>
                        ";
            // line 18
            echo ($context["schedule_menu"] ?? null);
            echo "</a>
                </div>
                <div class=\"col-md-3 col-sm-3 col-xs-3 text-center asx-footer\">
                    <a href=\"";
            // line 21
            echo ($context["more_href"] ?? null);
            echo "\">
                        <i class=\"fa fa-braille fa-2x\"></i><br/>
                        ";
            // line 23
            echo ($context["more_menu"] ?? null);
            echo "</a>
                </div>
            </div>
        </div>
        ";
            // line 28
            echo "        ";
            // line 29
            echo "        ";
            // line 30
            echo "        ";
            // line 31
            echo "        ";
            // line 32
            echo "        ";
            // line 33
            echo "        ";
            // line 34
            echo "        ";
            // line 35
            echo "        ";
            // line 36
            echo "        ";
            // line 37
            echo "        ";
            // line 38
            echo "        ";
            // line 39
            echo "        ";
            // line 40
            echo "        ";
            // line 41
            echo "        ";
            // line 42
            echo "        ";
            // line 43
            echo "        ";
            // line 44
            echo "        ";
            // line 45
            echo "        ";
            // line 46
            echo "        ";
            // line 47
            echo "        ";
            // line 48
            echo "        ";
            // line 49
            echo "        ";
            // line 50
            echo "        ";
            // line 51
            echo "        ";
            // line 52
            echo "        ";
            // line 53
            echo "        ";
            // line 54
            echo "        ";
            // line 55
            echo "        ";
            // line 56
            echo "        ";
            // line 57
            echo "        ";
            // line 58
            echo "        ";
            // line 59
            echo "        ";
            // line 60
            echo "        ";
            // line 61
            echo "        ";
            // line 62
            echo "        ";
            // line 63
            echo "        ";
            // line 64
            echo "        ";
            // line 65
            echo "        ";
            // line 66
            echo "        ";
            // line 67
            echo "        ";
            // line 68
            echo "        ";
            // line 69
            echo "    </footer>
";
        }
        // line 71
        echo "
";
        // line 73
        echo "    ";
        // line 76
        echo "    ";
        // line 78
        echo "</body></html>";
    }

    public function getTemplateName()
    {
        return "default/template/common/footer_askphoenix.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 78,  181 => 76,  179 => 73,  176 => 71,  172 => 69,  170 => 68,  168 => 67,  166 => 66,  164 => 65,  162 => 64,  160 => 63,  158 => 62,  156 => 61,  154 => 60,  152 => 59,  150 => 58,  148 => 57,  146 => 56,  144 => 55,  142 => 54,  140 => 53,  138 => 52,  136 => 51,  134 => 50,  132 => 49,  130 => 48,  128 => 47,  126 => 46,  124 => 45,  122 => 44,  120 => 43,  118 => 42,  116 => 41,  114 => 40,  112 => 39,  110 => 38,  108 => 37,  106 => 36,  104 => 35,  102 => 34,  100 => 33,  98 => 32,  96 => 31,  94 => 30,  92 => 29,  90 => 28,  83 => 23,  78 => 21,  72 => 18,  67 => 16,  61 => 13,  56 => 11,  50 => 8,  45 => 6,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/common/footer_askphoenix.twig", "");
    }
}

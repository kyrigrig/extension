<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/teacher/exams.twig */
class __TwigTemplate_88b8654337dfe4ef97c2f6bb63469412ec61d7f812a5eb36ef679bb783ba3e1a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div class=\"container-fluid asx-header-container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <h1><i class=\"fa fa-calendar\" style=\"margin-right:20px;\"></i>";
        // line 5
        echo ($context["heading_title"] ?? null);
        echo "</h1>
        </div>
    </div>
</div>
";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["options"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
            // line 10
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-exam-courses\" data-toggle=\"modal\" data-target=\"#myExamCourses\">
                <a href=\"";
            // line 13
            echo twig_get_attribute($this->env, $this->source, $context["option"], "menu_option", [], "any", false, false, false, 13);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["option"], "title_option", [], "any", false, false, false, 13);
            echo "</a>
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-exam-courses text-right\" data-toggle=\"modal\" data-target=\"#myExamCourses\">
                <a href=\"";
            // line 16
            echo twig_get_attribute($this->env, $this->source, $context["option"], "menu_option", [], "any", false, false, false, 16);
            echo "\">
                    ";
            // line 17
            if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_id", [], "any", false, false, false, 17)) {
                // line 18
                echo "                        ";
                echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 18);
                echo "
                    ";
            } else {
                // line 20
                echo "                        ";
                echo ($context["choice_text"] ?? null);
                echo "
                    ";
            }
            // line 22
            echo "                    &nbsp;&nbsp;<i class=\"fa fa-arrow-down\"></i>
                </a>
            </div>
        </div>
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "
";
        // line 29
        if (($context["pick_a_day"] ?? null)) {
            // line 30
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-pick-a-day\" data-toggle=\"modal\" data-target=\"#myExamLectures\">
                ";
            // line 33
            echo ($context["pick_a_day"] ?? null);
            echo "
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-pick-a-day text-right\">
                ";
            // line 36
            if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 36)) {
                // line 37
                echo "                    ";
                echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 37);
                echo "
                ";
            } else {
                // line 39
                echo "                    ";
                echo ($context["choice_text"] ?? null);
                echo "
                ";
            }
            // line 41
            echo "                <i class=\"fa fa-arrow-down\"></i>
            </div>
        </div>
    </div>
";
        }
        // line 46
        echo "
";
        // line 47
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_classroom_name", [], "any", false, false, false, 47)) {
            // line 48
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-pick-a-day\">";
            // line 50
            echo ($context["classroom_title"] ?? null);
            echo " ";
            echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_classroom_name", [], "any", false, false, false, 50);
            echo "</div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-classroom-name text-right\">";
            // line 51
            echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_lecture_start_date_time", [], "any", false, false, false, 51), "G:ia", "Europe/Athens");
            echo " - ";
            echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_lecture_end_date_time", [], "any", false, false, false, 51), "G:ia", "Europe/Athens");
            echo "</div>
        </div>
    </div>
";
        }
        // line 55
        echo "
";
        // line 56
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 56)) {
            // line 57
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12 asx-exam text-center\">
                <strong>";
            // line 60
            echo ($context["heading_add_exam_title"] ?? null);
            echo "</strong>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-exam\" data-toggle=\"modal\" data-target=\"#myExamEdit\">
                ";
            // line 65
            echo ($context["heading_add_exam_title"] ?? null);
            echo ": ";
            echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 65);
            echo "
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-exam text-right\" data-toggle=\"modal\" data-target=\"#myExamEdit\">
                ";
            // line 68
            echo ($context["edit_exam_btn_title"] ?? null);
            echo "&nbsp;<i class=\"fa fa-arrow-right\" title=\"";
            echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 68);
            echo "\"></i>
            </div>
        </div>
    </div>
";
        } else {
            // line 73
            echo "        <div class=\"container-fluid\">
            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12 col-xs-12 text-center\">
                    <p class=\"asx-mt-10\">";
            // line 76
            echo ($context["asx_lectures_exams_results"] ?? null);
            echo "</p>
                </div>
            </div>
        </div>
";
        }
        // line 81
        echo "
";
        // line 82
        if (($context["materials"] ?? null)) {
            // line 83
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            ";
            // line 85
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["materials"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["material_item"]) {
                // line 86
                echo "            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-exam\" data-toggle=\"modal\" data-target=\"#myExamMaterialEdit\">
                ";
                // line 87
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_book_name", [], "any", false, false, false, 87);
                echo "
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-exam text-right\" data-toggle=\"modal\" data-target=\"#myExamMaterialEdit\">
                ";
                // line 90
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_chapter", [], "any", false, false, false, 90);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_section", [], "any", false, false, false, 90);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_comments", [], "any", false, false, false, 90);
                echo "
                <i class=\"fa fa-arrow-right\" title=\"";
                // line 91
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_chapter", [], "any", false, false, false, 91);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_section", [], "any", false, false, false, 91);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_comments", [], "any", false, false, false, 91);
                echo "\"></i>
            </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['material_item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 94
            echo "        </div>
    </div>
";
        }
        // line 105
        echo "
";
        // line 106
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "lecture_id", [], "any", false, false, false, 106)) {
            // line 107
            echo "    <div class=\"menu\">
        <div class=\"btn trigger\">
            <span class=\"line\"></span>
        </div>
        <div class=\"icons\">
            ";
            // line 112
            if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 112)) {
                // line 113
                echo "                <div class=\"rotater\" data-toggle=\"modal\" data-target=\"#myAddExamMaterial\">
                    <div class=\"btn btn-icon\">
                        <i class=\"fa fa-pencil\" title=\"";
                // line 115
                echo ($context["add_exam_ili_title"] ?? null);
                echo "\"></i>
                    </div>
                </div>
            ";
            } else {
                // line 119
                echo "                <div class=\"rotater\" data-toggle=\"modal\" data-target=\"#myAddExam\">
                    <div class=\"btn btn-icon\">
                        <i class=\"fa fa-file\" title=\"";
                // line 121
                echo ($context["add_exam_title"] ?? null);
                echo "\"></i>
                    </div>
                </div>
            ";
            }
            // line 125
            echo "        </div>
    </div>
";
        }
        // line 128
        echo "
";
        // line 130
        echo "
<div class=\"modal fade\" id=\"myExamCourses\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">";
        // line 136
        echo ($context["modal_courses"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                ";
        // line 139
        if (($context["asx_results"] ?? null)) {
            // line 140
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["asx_results"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["result"]) {
                // line 141
                echo "                        <a href=\"";
                echo ($context["current_url"] ?? null);
                echo "&id=";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "id", [], "any", false, false, false, 141);
                echo "&course_name=";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "name", [], "any", false, false, false, 141);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "subCourse", [], "any", false, false, false, 141);
                echo " - ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "level", [], "any", false, false, false, 141);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "name", [], "any", false, false, false, 141);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "subCourse", [], "any", false, false, false, 141);
                echo " - ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "level", [], "any", false, false, false, 141);
                echo "</a><br/><br/>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 143
            echo "                ";
        }
        // line 144
        echo "            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 146
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myExamLectures\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModaLectureslLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModaLectureslLabel\">";
        // line 157
        echo ($context["pick_day_text"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                ";
        // line 160
        if (($context["asx_lectures_results"] ?? null)) {
            // line 161
            echo "                    <ul>
                        ";
            // line 162
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["asx_lectures_results"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["result_lecture"]) {
                // line 163
                echo "                            <li>
                                <a href=\"";
                // line 164
                echo ($context["current_url"] ?? null);
                echo "&id=";
                echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_id", [], "any", false, false, false, 164);
                echo "&lecture_id=";
                echo twig_get_attribute($this->env, $this->source, $context["result_lecture"], "id", [], "any", false, false, false, 164);
                echo "&date_format=";
                echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["result_lecture"], "startDateTime", [], "any", false, false, false, 164), "d/m/Y, D G:ia", "Europe/Athens");
                echo "\">";
                echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["result_lecture"], "startDateTime", [], "any", false, false, false, 164), "d/m/Y, D G:ia", "Europe/Athens");
                echo " (";
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["result_lecture"], "exam", [], "any", false, false, false, 164), "name", [], "any", false, false, false, 164);
                echo ")</a><br/><br/>
                            </li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result_lecture'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 167
            echo "                    </ul>
                ";
        }
        // line 169
        echo "            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 171
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myAddExamMaterial\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myAddExamMaterialLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myAddExamMaterialLabel\">";
        // line 182
        echo ($context["heading_add_study_title"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <form action=\"\" method=\"post\" role=\"form\" class=\"asx-form-books\">
                    <div class=\"modal-body\">
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 188
        echo ($context["course_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 189
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 189);
        echo "</div>
                            <input type=\"hidden\" name=\"course_name\" value=\"";
        // line 190
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 190);
        echo "\" />
                            <input type=\"hidden\" name=\"exam_id\" value=\"";
        // line 191
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_id", [], "any", false, false, false, 191);
        echo "\" />
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 194
        echo ($context["pick_day_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 195
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 195);
        echo "</div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 198
        echo ($context["modal_add_study_book_title"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">
                                <select name=\"book\" id=\"book\">
                                    ";
        // line 201
        if (($context["exam_study_books"] ?? null)) {
            // line 202
            echo "                                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["exam_study_books"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["exam_book_item"]) {
                // line 203
                echo "                                            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["exam_book_item"], "exam_study_books_id", [], "any", false, false, false, 203);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["exam_book_item"], "exam_study_books_name", [], "any", false, false, false, 203);
                echo "</option>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['exam_book_item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 205
            echo "                                    ";
        }
        // line 206
        echo "                                </select>
                            </div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 210
        echo ($context["modal_add_study_chapter_exam_name"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"chapter\" id=\"chapter\" /></div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 214
        echo ($context["modal_add_study_section_name"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"section\" id=\"section\" /></div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 218
        echo ($context["modal_add_study_comments_title"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"comments\" id=\"comments\" /></div>
                        </div>

                        <div class=\"col-md-8\"><input type=\"hidden\" name=\"submit_material_form\" id=\"submit_material_form\" value=\"1\" /></div>
                    </div>
                    <div class=\"modal-footer\">
                        <button class=\"btn btn-secondary asx-btn-modal\">";
        // line 225
        echo ($context["add_material_btn_title"] ?? null);
        echo " <i class=\"fa fa-paper-plane-o ml-1\"></i></button>
                    </div>
                </form>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 230
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myAddExam\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myAddExamLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myAddExamLabel\">";
        // line 241
        echo ($context["heading_add_exam_title"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <form action=\"\" method=\"post\" role=\"form\" class=\"asx-form-books\">
                    <div class=\"modal-body\">
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 247
        echo ($context["course_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 248
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 248);
        echo "</div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 251
        echo ($context["pick_day_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 252
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 252);
        echo "</div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 255
        echo ($context["modal_add_exam_name"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"exam_name\" id=\"exam_name\" /></div>
                        </div>
                        <div class=\"col-md-8\"><input type=\"hidden\" name=\"submit_form\" id=\"submit_form\" value=\"1\" /></div>
                    </div>
                    <div class=\"modal-footer\">
                        <button class=\"btn btn-secondary asx-btn-modal\">";
        // line 261
        echo ($context["add_exam_btn_title"] ?? null);
        echo " <i class=\"fa fa-paper-plane-o ml-1\"></i></button>
                    </div>
                </form>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 266
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myExamEdit\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myExamEdit\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myExamEdit\">";
        // line 277
        echo ($context["heading_add_exam_title"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <div class=\"form-group row\">
                    <div class=\"col-md-1 col-sm-1 col-xs-1\"><i class=\"fa fa-trash fa-2x\"></i></div>
                    <div class=\"col-md-11 col-sm-11 col-xs-11\" style=\"line-height: 32px;\">
                        <a href=\"";
        // line 283
        echo ($context["current_url"] ?? null);
        echo "&delete=";
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_id", [], "any", false, false, false, 283);
        echo "\">";
        echo ($context["edit_exam_delete_title"] ?? null);
        echo "</a>
                    </div>
                </div>
";
        // line 290
        echo "                <div class=\"form-group row\">
                    <div class=\"col-md-1 col-sm-1 col-xs-1\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><i class=\"fa fa-close fa-2x\"></i></div>
                    <div class=\"col-md-11 col-sm-11 col-xs-11\" data-dismiss=\"modal\" aria-label=\"Close\" style=\"line-height: 32px;\">";
        // line 292
        echo ($context["edit_exam_cancel_title"] ?? null);
        echo "</div>
                </div>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 296
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>



<script>
    \$(document).ready(function() {
        \$(\".trigger\").click(function() {
            \$(\".menu\").toggleClass(\"active\");
        });
    });
</script>

";
        // line 312
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/teacher/exams.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  648 => 312,  629 => 296,  622 => 292,  618 => 290,  608 => 283,  599 => 277,  585 => 266,  577 => 261,  568 => 255,  562 => 252,  558 => 251,  552 => 248,  548 => 247,  539 => 241,  525 => 230,  517 => 225,  507 => 218,  500 => 214,  493 => 210,  487 => 206,  484 => 205,  473 => 203,  468 => 202,  466 => 201,  460 => 198,  454 => 195,  450 => 194,  444 => 191,  440 => 190,  436 => 189,  432 => 188,  423 => 182,  409 => 171,  405 => 169,  401 => 167,  382 => 164,  379 => 163,  375 => 162,  372 => 161,  370 => 160,  364 => 157,  350 => 146,  346 => 144,  343 => 143,  320 => 141,  315 => 140,  313 => 139,  307 => 136,  299 => 130,  296 => 128,  291 => 125,  284 => 121,  280 => 119,  273 => 115,  269 => 113,  267 => 112,  260 => 107,  258 => 106,  255 => 105,  250 => 94,  237 => 91,  229 => 90,  223 => 87,  220 => 86,  216 => 85,  212 => 83,  210 => 82,  207 => 81,  199 => 76,  194 => 73,  184 => 68,  176 => 65,  168 => 60,  163 => 57,  161 => 56,  158 => 55,  149 => 51,  143 => 50,  139 => 48,  137 => 47,  134 => 46,  127 => 41,  121 => 39,  115 => 37,  113 => 36,  107 => 33,  102 => 30,  100 => 29,  97 => 28,  86 => 22,  80 => 20,  74 => 18,  72 => 17,  68 => 16,  60 => 13,  55 => 10,  51 => 9,  44 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/teacher/exams.twig", "");
    }
}

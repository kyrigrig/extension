<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/teacher/exams.twig */
class __TwigTemplate_8488d2ddf2fa943ff734b69f395e988594fe5c48b8a36508eb989f1a81025cff extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div class=\"container-fluid asx-header-container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <h1><i class=\"fa fa-calendar\" style=\"margin-right:20px;\"></i>";
        // line 5
        echo ($context["heading_title"] ?? null);
        echo "</h1>
        </div>
    </div>
</div>
";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["options"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
            // line 10
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-exam-courses\" data-toggle=\"modal\" data-target=\"#myExamCourses\">
                <a href=\"";
            // line 13
            echo twig_get_attribute($this->env, $this->source, $context["option"], "menu_option", [], "any", false, false, false, 13);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["option"], "title_option", [], "any", false, false, false, 13);
            echo "</a>
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-exam-courses text-right\" data-toggle=\"modal\" data-target=\"#myExamCourses\">
                <a href=\"";
            // line 16
            echo twig_get_attribute($this->env, $this->source, $context["option"], "menu_option", [], "any", false, false, false, 16);
            echo "\">
                    ";
            // line 17
            if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_id", [], "any", false, false, false, 17)) {
                // line 18
                echo "                        ";
                echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 18);
                echo "
                    ";
            } else {
                // line 20
                echo "                        ";
                echo ($context["choice_text"] ?? null);
                echo "
                    ";
            }
            // line 22
            echo "                    &nbsp;&nbsp;<i class=\"fa fa-arrow-down\"></i>
                </a>
            </div>
        </div>
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "
";
        // line 29
        if (($context["pick_a_day"] ?? null)) {
            // line 30
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-pick-a-day\" data-toggle=\"modal\" data-target=\"#myExamLectures\">
                ";
            // line 33
            echo ($context["pick_a_day"] ?? null);
            echo "
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-pick-a-day text-right\">
                ";
            // line 36
            if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 36)) {
                // line 37
                echo "                    ";
                echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 37);
                echo "
                ";
            } else {
                // line 39
                echo "                    ";
                echo ($context["choice_text"] ?? null);
                echo "
                ";
            }
            // line 41
            echo "                <i class=\"fa fa-arrow-down\"></i>
            </div>
        </div>
    </div>
";
        }
        // line 46
        echo "
";
        // line 47
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_classroom_name", [], "any", false, false, false, 47)) {
            // line 48
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-pick-a-day\">";
            // line 50
            echo ($context["classroom_title"] ?? null);
            echo " ";
            echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_classroom_name", [], "any", false, false, false, 50);
            echo "</div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-classroom-name text-right\">";
            // line 51
            echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_lecture_start_date_time", [], "any", false, false, false, 51), "G:ia", "Europe/Athens");
            echo " - ";
            echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_lecture_end_date_time", [], "any", false, false, false, 51), "G:ia", "Europe/Athens");
            echo "</div>
        </div>
    </div>
";
        }
        // line 55
        echo "
";
        // line 56
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 56)) {
            // line 57
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12 asx-exam text-center\">
                <strong>";
            // line 60
            echo ($context["heading_add_exam_title"] ?? null);
            echo "</strong>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-exam\" data-toggle=\"modal\" data-target=\"#myExamEdit\">
                ";
            // line 65
            echo ($context["heading_add_exam_title"] ?? null);
            echo ": ";
            echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 65);
            echo "
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-exam text-right\" data-toggle=\"modal\" data-target=\"#myExamEdit\">
                ";
            // line 68
            echo ($context["edit_exam_btn_title"] ?? null);
            echo "&nbsp;<i class=\"fa fa-arrow-right\" title=\"";
            echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 68);
            echo "\"></i>
            </div>
        </div>
    </div>
";
        } else {
            // line 73
            echo "        <div class=\"container-fluid\">
            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12 col-xs-12 text-center\">
                    <p class=\"asx-mt-10\">";
            // line 76
            echo ($context["asx_lectures_exams_results"] ?? null);
            echo "</p>
                </div>
            </div>
        </div>
";
        }
        // line 81
        echo "
";
        // line 82
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "materials", [], "any", false, false, false, 82)) {
            // line 83
            echo "    ";
            echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "materials", [], "any", false, false, false, 83);
            echo "
    <div class=\"container-fluid\">
";
            // line 90
            echo "        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-exam\" data-toggle=\"modal\" data-target=\"#myExamMaterialEdit\">
                ";
            // line 92
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "materials", [], "any", false, false, false, 92), "material_book_name", [], "any", false, false, false, 92);
            echo "
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-exam text-right\" data-toggle=\"modal\" data-target=\"#myExamMaterialEdit\">
                <i class=\"fa fa-arrow-right\" title=\"";
            // line 95
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "materials", [], "any", false, false, false, 95), "material_chapter", [], "any", false, false, false, 95);
            echo ", ";
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "materials", [], "any", false, false, false, 95), "material_section", [], "any", false, false, false, 95);
            echo ", ";
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "materials", [], "any", false, false, false, 95), "material_comments", [], "any", false, false, false, 95);
            echo "\"></i>
            </div>
        </div>
    </div>
";
        }
        // line 108
        echo "
";
        // line 109
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "lecture_id", [], "any", false, false, false, 109)) {
            // line 110
            echo "    <div class=\"menu\">
        <div class=\"btn trigger\">
            <span class=\"line\"></span>
        </div>
        <div class=\"icons\">
            ";
            // line 115
            if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 115)) {
                // line 116
                echo "                <div class=\"rotater\" data-toggle=\"modal\" data-target=\"#myAddExamMaterial\">
                    <div class=\"btn btn-icon\">
                        <i class=\"fa fa-pencil\" title=\"";
                // line 118
                echo ($context["add_exam_ili_title"] ?? null);
                echo "\"></i>
                    </div>
                </div>
            ";
            } else {
                // line 122
                echo "                <div class=\"rotater\" data-toggle=\"modal\" data-target=\"#myAddExam\">
                    <div class=\"btn btn-icon\">
                        <i class=\"fa fa-file\" title=\"";
                // line 124
                echo ($context["add_exam_title"] ?? null);
                echo "\"></i>
                    </div>
                </div>
            ";
            }
            // line 128
            echo "        </div>
    </div>
";
        }
        // line 131
        echo "
";
        // line 133
        echo "
<div class=\"modal fade\" id=\"myExamCourses\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">";
        // line 139
        echo ($context["modal_courses"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                ";
        // line 142
        if (($context["asx_results"] ?? null)) {
            // line 143
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["asx_results"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["result"]) {
                // line 144
                echo "                        <a href=\"";
                echo ($context["current_url"] ?? null);
                echo "&id=";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "id", [], "any", false, false, false, 144);
                echo "&course_name=";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "name", [], "any", false, false, false, 144);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "subCourse", [], "any", false, false, false, 144);
                echo " - ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "level", [], "any", false, false, false, 144);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "name", [], "any", false, false, false, 144);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "subCourse", [], "any", false, false, false, 144);
                echo " - ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "level", [], "any", false, false, false, 144);
                echo "</a><br/><br/>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 146
            echo "                ";
        }
        // line 147
        echo "            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 149
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myExamLectures\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModaLectureslLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModaLectureslLabel\">";
        // line 160
        echo ($context["pick_day_text"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                ";
        // line 163
        if (($context["asx_lectures_results"] ?? null)) {
            // line 164
            echo "                    <ul>
                        ";
            // line 165
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["asx_lectures_results"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["result_lecture"]) {
                // line 166
                echo "                            <li>
                                <a href=\"";
                // line 167
                echo ($context["current_url"] ?? null);
                echo "&id=";
                echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_id", [], "any", false, false, false, 167);
                echo "&lecture_id=";
                echo twig_get_attribute($this->env, $this->source, $context["result_lecture"], "id", [], "any", false, false, false, 167);
                echo "&date_format=";
                echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["result_lecture"], "startDateTime", [], "any", false, false, false, 167), "d/m/Y, D G:ia", "Europe/Athens");
                echo "\">";
                echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["result_lecture"], "startDateTime", [], "any", false, false, false, 167), "d/m/Y, D G:ia", "Europe/Athens");
                echo " (";
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["result_lecture"], "exam", [], "any", false, false, false, 167), "name", [], "any", false, false, false, 167);
                echo ")</a><br/><br/>
                            </li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result_lecture'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 170
            echo "                    </ul>
                ";
        }
        // line 172
        echo "            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 174
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myAddExamMaterial\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myAddExamMaterialLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myAddExamMaterialLabel\">";
        // line 185
        echo ($context["heading_add_study_title"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <form action=\"\" method=\"post\" role=\"form\" class=\"asx-form-books\">
                    <div class=\"modal-body\">
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 191
        echo ($context["course_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 192
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 192);
        echo "</div>
                            <input type=\"hidden\" name=\"course_name\" value=\"";
        // line 193
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 193);
        echo "\" />
                            <input type=\"hidden\" name=\"exam_id\" value=\"";
        // line 194
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_id", [], "any", false, false, false, 194);
        echo "\" />
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 197
        echo ($context["pick_day_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 198
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 198);
        echo "</div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 201
        echo ($context["modal_add_study_book_title"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">
                                <select name=\"book\" id=\"book\">
                                    ";
        // line 204
        if (($context["exam_study_books"] ?? null)) {
            // line 205
            echo "                                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["exam_study_books"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["exam_book_item"]) {
                // line 206
                echo "                                            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["exam_book_item"], "exam_study_books_id", [], "any", false, false, false, 206);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["exam_book_item"], "exam_study_books_name", [], "any", false, false, false, 206);
                echo "</option>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['exam_book_item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 208
            echo "                                    ";
        }
        // line 209
        echo "                                </select>
                            </div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 213
        echo ($context["modal_add_study_chapter_exam_name"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"chapter\" id=\"chapter\" /></div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 217
        echo ($context["modal_add_study_section_name"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"section\" id=\"section\" /></div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 221
        echo ($context["modal_add_study_comments_title"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"comments\" id=\"comments\" /></div>
                        </div>

                        <div class=\"col-md-8\"><input type=\"hidden\" name=\"submit_material_form\" id=\"submit_material_form\" value=\"1\" /></div>
                    </div>
                    <div class=\"modal-footer\">
                        <button class=\"btn btn-secondary asx-btn-modal\">";
        // line 228
        echo ($context["add_material_btn_title"] ?? null);
        echo " <i class=\"fa fa-paper-plane-o ml-1\"></i></button>
                    </div>
                </form>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 233
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myAddExam\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myAddExamLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myAddExamLabel\">";
        // line 244
        echo ($context["heading_add_exam_title"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <form action=\"\" method=\"post\" role=\"form\" class=\"asx-form-books\">
                    <div class=\"modal-body\">
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 250
        echo ($context["course_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 251
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 251);
        echo "</div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 254
        echo ($context["pick_day_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 255
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 255);
        echo "</div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 258
        echo ($context["modal_add_exam_name"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"exam_name\" id=\"exam_name\" /></div>
                        </div>
                        <div class=\"col-md-8\"><input type=\"hidden\" name=\"submit_form\" id=\"submit_form\" value=\"1\" /></div>
                    </div>
                    <div class=\"modal-footer\">
                        <button class=\"btn btn-secondary asx-btn-modal\">";
        // line 264
        echo ($context["add_exam_btn_title"] ?? null);
        echo " <i class=\"fa fa-paper-plane-o ml-1\"></i></button>
                    </div>
                </form>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 269
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myExamEdit\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myExamEdit\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myExamEdit\">";
        // line 280
        echo ($context["heading_add_exam_title"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <div class=\"form-group row\">
                    <div class=\"col-md-1 col-sm-1 col-xs-1\"><i class=\"fa fa-trash fa-2x\"></i></div>
                    <div class=\"col-md-11 col-sm-11 col-xs-11\" style=\"line-height: 32px;\">
                        <a href=\"";
        // line 286
        echo ($context["current_url"] ?? null);
        echo "&delete=";
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_id", [], "any", false, false, false, 286);
        echo "\">";
        echo ($context["edit_exam_delete_title"] ?? null);
        echo "</a>
                    </div>
                </div>
";
        // line 293
        echo "                <div class=\"form-group row\">
                    <div class=\"col-md-1 col-sm-1 col-xs-1\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><i class=\"fa fa-close fa-2x\"></i></div>
                    <div class=\"col-md-11 col-sm-11 col-xs-11\" data-dismiss=\"modal\" aria-label=\"Close\" style=\"line-height: 32px;\">";
        // line 295
        echo ($context["edit_exam_cancel_title"] ?? null);
        echo "</div>
                </div>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 299
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>



<script>
    \$(document).ready(function() {
        \$(\".trigger\").click(function() {
            \$(\".menu\").toggleClass(\"active\");
        });
    });
</script>

";
        // line 315
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/teacher/exams.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  633 => 315,  614 => 299,  607 => 295,  603 => 293,  593 => 286,  584 => 280,  570 => 269,  562 => 264,  553 => 258,  547 => 255,  543 => 254,  537 => 251,  533 => 250,  524 => 244,  510 => 233,  502 => 228,  492 => 221,  485 => 217,  478 => 213,  472 => 209,  469 => 208,  458 => 206,  453 => 205,  451 => 204,  445 => 201,  439 => 198,  435 => 197,  429 => 194,  425 => 193,  421 => 192,  417 => 191,  408 => 185,  394 => 174,  390 => 172,  386 => 170,  367 => 167,  364 => 166,  360 => 165,  357 => 164,  355 => 163,  349 => 160,  335 => 149,  331 => 147,  328 => 146,  305 => 144,  300 => 143,  298 => 142,  292 => 139,  284 => 133,  281 => 131,  276 => 128,  269 => 124,  265 => 122,  258 => 118,  254 => 116,  252 => 115,  245 => 110,  243 => 109,  240 => 108,  228 => 95,  222 => 92,  218 => 90,  212 => 83,  210 => 82,  207 => 81,  199 => 76,  194 => 73,  184 => 68,  176 => 65,  168 => 60,  163 => 57,  161 => 56,  158 => 55,  149 => 51,  143 => 50,  139 => 48,  137 => 47,  134 => 46,  127 => 41,  121 => 39,  115 => 37,  113 => 36,  107 => 33,  102 => 30,  100 => 29,  97 => 28,  86 => 22,  80 => 20,  74 => 18,  72 => 17,  68 => 16,  60 => 13,  55 => 10,  51 => 9,  44 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/teacher/exams.twig", "");
    }
}

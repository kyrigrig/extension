<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/teacher/exams.twig */
class __TwigTemplate_fe9bd18470b932696ccf4ba95f6d2a036ffe68888befc62a403553b8efef4d73 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div class=\"container-fluid asx-header-container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <h1><i class=\"fa fa-calendar\" style=\"margin-right:20px;\"></i>";
        // line 5
        echo ($context["heading_title"] ?? null);
        echo "</h1>
        </div>
    </div>
</div>
";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["options"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
            // line 10
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-exam-courses\" data-toggle=\"modal\" data-target=\"#myExamCourses\">
                <a href=\"";
            // line 13
            echo twig_get_attribute($this->env, $this->source, $context["option"], "menu_option", [], "any", false, false, false, 13);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["option"], "title_option", [], "any", false, false, false, 13);
            echo "</a>
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-exam-courses text-right\" data-toggle=\"modal\" data-target=\"#myExamCourses\">
                <a href=\"";
            // line 16
            echo twig_get_attribute($this->env, $this->source, $context["option"], "menu_option", [], "any", false, false, false, 16);
            echo "\">
                    ";
            // line 17
            if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_id", [], "any", false, false, false, 17)) {
                // line 18
                echo "                        ";
                echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 18);
                echo "
                    ";
            } else {
                // line 20
                echo "                        ";
                echo ($context["choice_text"] ?? null);
                echo "
                    ";
            }
            // line 22
            echo "                    &nbsp;&nbsp;<i class=\"fa fa-arrow-down\"></i>
                </a>
            </div>
        </div>
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "
";
        // line 29
        if (($context["pick_a_day"] ?? null)) {
            // line 30
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-pick-a-day\" data-toggle=\"modal\" data-target=\"#myExamLectures\">
                ";
            // line 33
            echo ($context["pick_a_day"] ?? null);
            echo "
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-pick-a-day text-right\">
                ";
            // line 36
            if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 36)) {
                // line 37
                echo "                    ";
                echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 37);
                echo "
                ";
            } else {
                // line 39
                echo "                    ";
                echo ($context["choice_text"] ?? null);
                echo "
                ";
            }
            // line 41
            echo "                <i class=\"fa fa-arrow-down\"></i>
            </div>
        </div>
    </div>
";
        }
        // line 46
        echo "
";
        // line 47
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_classroom_name", [], "any", false, false, false, 47)) {
            // line 48
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-pick-a-day\">";
            // line 50
            echo ($context["classroom_title"] ?? null);
            echo " ";
            echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_classroom_name", [], "any", false, false, false, 50);
            echo "</div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-classroom-name text-right\">";
            // line 51
            echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_lecture_start_date_time", [], "any", false, false, false, 51), "G:ia", "Europe/Athens");
            echo " - ";
            echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_lecture_end_date_time", [], "any", false, false, false, 51), "G:ia", "Europe/Athens");
            echo "</div>
        </div>
    </div>
";
        }
        // line 55
        echo "
";
        // line 56
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 56)) {
            // line 57
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12 asx-exam text-center\">
                <strong>";
            // line 60
            echo ($context["heading_add_exam_title"] ?? null);
            echo "</strong>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-exam\" data-toggle=\"modal\" data-target=\"#myExamEdit\">
                ";
            // line 65
            echo ($context["heading_add_exam_title"] ?? null);
            echo ": ";
            echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 65);
            echo "
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-exam text-right\" data-toggle=\"modal\" data-target=\"#myExamEdit\">
                ";
            // line 68
            echo ($context["edit_exam_btn_title"] ?? null);
            echo "&nbsp;<i class=\"fa fa-arrow-right\" title=\"";
            echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 68);
            echo "\"></i>
            </div>
        </div>
    </div>
";
        } else {
            // line 73
            echo "        <div class=\"container-fluid\">
            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12 col-xs-12 text-center\">
                    <p class=\"asx-mt-10\">";
            // line 76
            echo ($context["asx_lectures_exams_results"] ?? null);
            echo "</p>
                </div>
            </div>
        </div>
";
        }
        // line 81
        echo "
";
        // line 82
        if (($context["materials"] ?? null)) {
            // line 83
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            ";
            // line 85
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["materials"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["material_item"]) {
                // line 86
                echo "            <div class=\"col-md-9 col-sm-12 col-xs-12 asx-exam asx-exam-material\" data-toggle=\"modal\" data-target=\"#myExamMaterialEdit";
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_id", [], "any", false, false, false, 86);
                echo "\">
                ";
                // line 87
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_book_name", [], "any", false, false, false, 87);
                echo "
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12 asx-exam asx-exam-material text-right\" data-toggle=\"modal\" data-target=\"#myExamMaterialEdit";
                // line 89
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_id", [], "any", false, false, false, 89);
                echo "\">
                ";
                // line 90
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_chapter", [], "any", false, false, false, 90);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_section", [], "any", false, false, false, 90);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_comments", [], "any", false, false, false, 90);
                echo "
                <i class=\"fa fa-arrow-right\" title=\"";
                // line 91
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_chapter", [], "any", false, false, false, 91);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_section", [], "any", false, false, false, 91);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_comments", [], "any", false, false, false, 91);
                echo "\"></i>
            </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['material_item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 94
            echo "        </div>
    </div>
";
        }
        // line 97
        echo "
";
        // line 98
        if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "lecture_id", [], "any", false, false, false, 98)) {
            // line 99
            echo "    <div class=\"menu\">
        <div class=\"btn trigger\">
            <span class=\"line\"></span>
        </div>
        <div class=\"icons\">
            ";
            // line 104
            if (twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_name", [], "any", false, false, false, 104)) {
                // line 105
                echo "                <div class=\"rotater\" data-toggle=\"modal\" data-target=\"#myAddExamMaterial\">
                    <div class=\"btn btn-icon\">
                        <i class=\"fa fa-pencil\" title=\"";
                // line 107
                echo ($context["add_exam_ili_title"] ?? null);
                echo "\"></i>
                    </div>
                </div>
            ";
            } else {
                // line 111
                echo "                <div class=\"rotater\" data-toggle=\"modal\" data-target=\"#myAddExam\">
                    <div class=\"btn btn-icon\">
                        <i class=\"fa fa-file\" title=\"";
                // line 113
                echo ($context["add_exam_title"] ?? null);
                echo "\"></i>
                    </div>
                </div>
            ";
            }
            // line 117
            echo "        </div>
    </div>
";
        }
        // line 120
        echo "
";
        // line 122
        echo "
<div class=\"modal fade\" id=\"myExamCourses\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">";
        // line 128
        echo ($context["modal_courses"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                ";
        // line 131
        if (($context["asx_results"] ?? null)) {
            // line 132
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["asx_results"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["result"]) {
                // line 133
                echo "                        <a href=\"";
                echo ($context["current_url"] ?? null);
                echo "&id=";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "id", [], "any", false, false, false, 133);
                echo "&course_name=";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "name", [], "any", false, false, false, 133);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "subCourse", [], "any", false, false, false, 133);
                echo " - ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "level", [], "any", false, false, false, 133);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "name", [], "any", false, false, false, 133);
                echo ", ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "subCourse", [], "any", false, false, false, 133);
                echo " - ";
                echo twig_get_attribute($this->env, $this->source, $context["result"], "level", [], "any", false, false, false, 133);
                echo "</a><br/><br/>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 135
            echo "                ";
        }
        // line 136
        echo "            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 138
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myExamLectures\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModaLectureslLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModaLectureslLabel\">";
        // line 149
        echo ($context["pick_day_text"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                ";
        // line 152
        if (($context["asx_lectures_results"] ?? null)) {
            // line 153
            echo "                    <ul>
                        ";
            // line 154
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["asx_lectures_results"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["result_lecture"]) {
                // line 155
                echo "                            <li>
                                <a href=\"";
                // line 156
                echo ($context["current_url"] ?? null);
                echo "&id=";
                echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_id", [], "any", false, false, false, 156);
                echo "&lecture_id=";
                echo twig_get_attribute($this->env, $this->source, $context["result_lecture"], "id", [], "any", false, false, false, 156);
                echo "&date_format=";
                echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["result_lecture"], "startDateTime", [], "any", false, false, false, 156), "d/m/Y, D G:ia", "Europe/Athens");
                echo "\">
                                    ";
                // line 157
                echo twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["result_lecture"], "startDateTime", [], "any", false, false, false, 157), "d/m/Y, D G:ia", "Europe/Athens");
                echo " (";
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["result_lecture"], "exam", [], "any", false, false, false, 157), "name", [], "any", false, false, false, 157);
                echo ")
                                </a><br/><br/>
                            </li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result_lecture'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 161
            echo "                    </ul>
                ";
        }
        // line 163
        echo "            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 165
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myAddExamMaterial\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myAddExamMaterialLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myAddExamMaterialLabel\">";
        // line 176
        echo ($context["heading_add_study_title"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <form action=\"\" method=\"post\" role=\"form\" class=\"asx-form-books\">
                    <div class=\"modal-body\">
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 182
        echo ($context["course_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 183
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 183);
        echo "</div>
                            <input type=\"hidden\" name=\"course_name\" value=\"";
        // line 184
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 184);
        echo "\" />
                            <input type=\"hidden\" name=\"exam_id\" value=\"";
        // line 185
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_id", [], "any", false, false, false, 185);
        echo "\" />
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 188
        echo ($context["pick_day_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 189
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 189);
        echo "</div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 192
        echo ($context["modal_add_study_book_title"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">
                                <select name=\"book\" id=\"book\">
                                    ";
        // line 195
        if (($context["exam_study_books"] ?? null)) {
            // line 196
            echo "                                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["exam_study_books"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["exam_book_item"]) {
                // line 197
                echo "                                            <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["exam_book_item"], "exam_study_books_id", [], "any", false, false, false, 197);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["exam_book_item"], "exam_study_books_name", [], "any", false, false, false, 197);
                echo "</option>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['exam_book_item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 199
            echo "                                    ";
        }
        // line 200
        echo "                                </select>
                            </div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 204
        echo ($context["modal_add_study_chapter_exam_name"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"chapter\" id=\"chapter\" /></div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 208
        echo ($context["modal_add_study_section_name"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"section\" id=\"section\" /></div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 212
        echo ($context["modal_add_study_comments_title"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"comments\" id=\"comments\" /></div>
                        </div>

                        <div class=\"col-md-8\"><input type=\"hidden\" name=\"submit_material_form\" id=\"submit_material_form\" value=\"1\" /></div>
                    </div>
                    <div class=\"modal-footer\">
                        <button class=\"btn btn-secondary asx-btn-modal\">";
        // line 219
        echo ($context["add_material_btn_title"] ?? null);
        echo " <i class=\"fa fa-paper-plane-o ml-1\"></i></button>
                    </div>
                </form>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 224
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myAddExam\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myAddExamLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myAddExamLabel\">";
        // line 235
        echo ($context["heading_add_exam_title"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <form action=\"\" method=\"post\" role=\"form\" class=\"asx-form-books\">
                    <div class=\"modal-body\">
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 241
        echo ($context["course_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 242
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "course_name", [], "any", false, false, false, 242);
        echo "</div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 245
        echo ($context["pick_day_text"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\">";
        // line 246
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "date_format", [], "any", false, false, false, 246);
        echo "</div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-4\">";
        // line 249
        echo ($context["modal_add_exam_name"] ?? null);
        echo ":</div>
                            <div class=\"col-md-8 text-right\"><input type=\"text\" name=\"exam_name\" id=\"exam_name\" /></div>
                        </div>
                        <div class=\"col-md-8\"><input type=\"hidden\" name=\"submit_form\" id=\"submit_form\" value=\"1\" /></div>
                    </div>
                    <div class=\"modal-footer\">
                        <button class=\"btn btn-secondary asx-btn-modal\">";
        // line 255
        echo ($context["add_exam_btn_title"] ?? null);
        echo " <i class=\"fa fa-paper-plane-o ml-1\"></i></button>
                    </div>
                </form>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 260
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

<div class=\"modal fade\" id=\"myExamEdit\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myExamEdit\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myExamEdit\">";
        // line 271
        echo ($context["heading_add_exam_title"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <div class=\"form-group row\">
                    <div class=\"col-md-1 col-sm-1 col-xs-1\"><i class=\"fa fa-trash fa-2x\"></i></div>
                    <div class=\"col-md-11 col-sm-11 col-xs-11\" style=\"line-height: 32px;\">
                        <a href=\"";
        // line 277
        echo ($context["current_url"] ?? null);
        echo "&delete=";
        echo twig_get_attribute($this->env, $this->source, ($context["display_selected"] ?? null), "exam_id", [], "any", false, false, false, 277);
        echo "\">";
        echo ($context["edit_exam_delete_title"] ?? null);
        echo "</a>
                    </div>
                </div>
";
        // line 284
        echo "                <div class=\"form-group row\">
                    <div class=\"col-md-1 col-sm-1 col-xs-1\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><i class=\"fa fa-close fa-2x\"></i></div>
                    <div class=\"col-md-11 col-sm-11 col-xs-11\" data-dismiss=\"modal\" aria-label=\"Close\" style=\"line-height: 32px;\">";
        // line 286
        echo ($context["edit_exam_cancel_title"] ?? null);
        echo "</div>
                </div>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 290
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>

";
        // line 296
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["materials"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["material_item"]) {
            // line 297
            echo "<div class=\"modal fade asx-dynamic-materials\" id=\"myExamMaterialEdit";
            echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_id", [], "any", false, false, false, 297);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myExamMaterialEdit";
            echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_id", [], "any", false, false, false, 297);
            echo "\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myExamMaterialEdit";
            // line 302
            echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_id", [], "any", false, false, false, 302);
            echo "\">";
            echo ($context["heading_add_study_title"] ?? null);
            echo "</h4>
            </div>
            <div class=\"modal-body\">
                <div class=\"form-group row\">
                    <div class=\"col-md-1 col-sm-1 col-xs-1\"><i class=\"fa fa-trash fa-2x\"></i></div>
                    <div class=\"col-md-11 col-sm-11 col-xs-11\" style=\"line-height: 32px;\">
                        <a href=\"";
            // line 308
            echo ($context["current_url"] ?? null);
            echo "&delete_material=";
            echo twig_get_attribute($this->env, $this->source, $context["material_item"], "material_id", [], "any", false, false, false, 308);
            echo "\">";
            echo ($context["edit_exam_material_delete_title"] ?? null);
            echo "</a>
                    </div>
                </div>
                <div class=\"form-group row\">
                    <div class=\"col-md-1 col-sm-1 col-xs-1\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><i class=\"fa fa-close fa-2x\"></i></div>
                    <div class=\"col-md-11 col-sm-11 col-xs-11\" data-dismiss=\"modal\" aria-label=\"Close\" style=\"line-height: 32px;\">";
            // line 313
            echo ($context["edit_exam_material_cancel_title"] ?? null);
            echo "</div>
                </div>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
            // line 317
            echo ($context["modal_close"] ?? null);
            echo "</button>
            </div>
        </div>
    </div>
</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['material_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 323
        echo "


<script>
    \$(document).ready(function() {
        \$(\".trigger\").click(function() {
            \$(\".menu\").toggleClass(\"active\");
        });
    });
</script>

";
        // line 334
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/teacher/exams.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  716 => 334,  703 => 323,  691 => 317,  684 => 313,  672 => 308,  661 => 302,  650 => 297,  646 => 296,  637 => 290,  630 => 286,  626 => 284,  616 => 277,  607 => 271,  593 => 260,  585 => 255,  576 => 249,  570 => 246,  566 => 245,  560 => 242,  556 => 241,  547 => 235,  533 => 224,  525 => 219,  515 => 212,  508 => 208,  501 => 204,  495 => 200,  492 => 199,  481 => 197,  476 => 196,  474 => 195,  468 => 192,  462 => 189,  458 => 188,  452 => 185,  448 => 184,  444 => 183,  440 => 182,  431 => 176,  417 => 165,  413 => 163,  409 => 161,  397 => 157,  387 => 156,  384 => 155,  380 => 154,  377 => 153,  375 => 152,  369 => 149,  355 => 138,  351 => 136,  348 => 135,  325 => 133,  320 => 132,  318 => 131,  312 => 128,  304 => 122,  301 => 120,  296 => 117,  289 => 113,  285 => 111,  278 => 107,  274 => 105,  272 => 104,  265 => 99,  263 => 98,  260 => 97,  255 => 94,  242 => 91,  234 => 90,  230 => 89,  225 => 87,  220 => 86,  216 => 85,  212 => 83,  210 => 82,  207 => 81,  199 => 76,  194 => 73,  184 => 68,  176 => 65,  168 => 60,  163 => 57,  161 => 56,  158 => 55,  149 => 51,  143 => 50,  139 => 48,  137 => 47,  134 => 46,  127 => 41,  121 => 39,  115 => 37,  113 => 36,  107 => 33,  102 => 30,  100 => 29,  97 => 28,  86 => 22,  80 => 20,  74 => 18,  72 => 17,  68 => 16,  60 => 13,  55 => 10,  51 => 9,  44 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/teacher/exams.twig", "");
    }
}

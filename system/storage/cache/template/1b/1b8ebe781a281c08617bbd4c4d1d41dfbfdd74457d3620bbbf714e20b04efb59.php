<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/teacher/account.twig */
class __TwigTemplate_d7259a97d919ad00b70d29f59181dd49f2a1ffd6b734bdc558d02b2867cf6a1e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "

";
        // line 3
        if (($context["asx_message"] ?? null)) {
            // line 4
            echo "    <script>
        Swal.fire({
            icon: 'info',
            text: '";
            // line 7
            echo ($context["asx_message"] ?? null);
            echo "'
        })
    </script>
";
        }
        // line 11
        echo "
<div class=\"container-fluid asx-header-container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <h1>
                <a href=\"";
        // line 16
        echo ($context["previous_current_url"] ?? null);
        echo "\">
                    <i class=\"fa fa-arrow-left fa-icon-hover\" style=\"margin-right:20px;font-size: 24px;width: fit-content;\"></i>
                </a>
                ";
        // line 19
        echo ($context["heading_title"] ?? null);
        echo "
            </h1>
        </div>
    </div>
</div>

";
        // line 26
        if (($context["account_info"] ?? null)) {
            // line 27
            echo "    <div class=\"container-fluid asx-account-info\">
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12 asx-account asx-account-item\">
                <label>";
            // line 30
            echo ($context["account_last_name"] ?? null);
            echo ":</label><br />
                <a href=\"javascript:void(0);\">";
            // line 31
            echo twig_get_attribute($this->env, $this->source, ($context["account_info"] ?? null), "last_name", [], "any", false, false, false, 31);
            echo "</a>
            </div>
            <div class=\"col-md-12 col-sm-12 col-xs-12 asx-account asx-account-item\">
                <label>";
            // line 34
            echo ($context["account_first_name"] ?? null);
            echo ":</label><br />
                <a href=\"javascript:void(0);\">";
            // line 35
            echo twig_get_attribute($this->env, $this->source, ($context["account_info"] ?? null), "first_name", [], "any", false, false, false, 35);
            echo "</a>
            </div>
            <div class=\"col-md-12 col-sm-12 col-xs-12 asx-account asx-account-item\">
                <label>";
            // line 38
            echo ($context["account_phone_number"] ?? null);
            echo ":</label><br />
                <a href=\"javascript:void(0);\">";
            // line 39
            echo twig_get_attribute($this->env, $this->source, ($context["account_info"] ?? null), "phone_number", [], "any", false, false, false, 39);
            echo "</a>
            </div>
            <div class=\"col-md-12 col-sm-12 col-xs-12 asx-account asx-account-item\">
                <label>";
            // line 42
            echo ($context["account_email"] ?? null);
            echo ":</label><br />
                <a href=\"javascript:void(0);\">";
            // line 43
            echo twig_get_attribute($this->env, $this->source, ($context["account_info"] ?? null), "email_confirmed", [], "any", false, false, false, 43);
            echo "</a>
            </div>
            <div class=\"col-md-12 col-sm-12 col-xs-12 asx-account\" data-toggle=\"modal\" data-target=\"#myAccountInfo\">
                <p>";
            // line 46
            echo ($context["account_change_password"] ?? null);
            echo "</p>
            </div>
        </div>
    </div>
";
        }
        // line 51
        echo "
<div class=\"modal fade\" id=\"myAccountInfo\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myAccountInfo\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myAccountInfo\">";
        // line 57
        echo ($context["account_change_password_title"] ?? null);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <form action=\"\" method=\"post\" role=\"form\" class=\"asx-form-change-password\">
                    <div class=\"modal-body\">
                        <div class=\"form-group row\">
                            <div class=\"col-md-12\">";
        // line 63
        echo ($context["account_old_password"] ?? null);
        echo ":<br />
                                <input type=\"password\" name=\"oldPassword\" id=\"oldPassword\" />
                            </div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-12\">";
        // line 68
        echo ($context["account_new_password"] ?? null);
        echo ":<br />
                                <input type=\"password\" name=\"newPassword\" id=\"newPassword\" />
                            </div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-12\">";
        // line 73
        echo ($context["account_confirm_password"] ?? null);
        echo ":<br />
                                <input type=\"password\" name=\"confirm_password\" id=\"confirm_password\" />
                            </div>
                        </div>
                        <div class=\"form-group row\">
                            <div class=\"col-md-12\">
                                <input type=\"submit\" name=\"submit_change_password\" id=\"submit_change_password\" value=\"";
        // line 79
        echo ($context["account_change_password"] ?? null);
        echo "\" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 86
        echo ($context["modal_close"] ?? null);
        echo "</button>
            </div>
        </div>
    </div>
</div>


";
        // line 93
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/teacher/account.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  195 => 93,  185 => 86,  175 => 79,  166 => 73,  158 => 68,  150 => 63,  141 => 57,  133 => 51,  125 => 46,  119 => 43,  115 => 42,  109 => 39,  105 => 38,  99 => 35,  95 => 34,  89 => 31,  85 => 30,  80 => 27,  78 => 26,  69 => 19,  63 => 16,  56 => 11,  49 => 7,  44 => 4,  42 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/teacher/account.twig", "");
    }
}

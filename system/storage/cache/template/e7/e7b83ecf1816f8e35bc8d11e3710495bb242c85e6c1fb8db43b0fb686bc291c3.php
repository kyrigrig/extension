<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/account/login.twig */
class __TwigTemplate_16dedea38a17c322642a0d4fe2bcf77e9e3597bfaeacc96039237c601d6897a5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header_askphoenix"] ?? null);
        echo "
<body>
\t<div class=\"container-fluid asx-login\">
\t\t<div class=\"row asx-header-login\">
\t\t\t<h3>";
        // line 5
        echo ($context["heading_title"] ?? null);
        echo "</h3>
\t\t</div>
\t\t<div class=\"row text-center\">
\t\t\t<div class=\"asx-logo\">
\t\t\t\t<img src=\"https://staging.askphoenix.gr/extension/image/icon.png\" />
\t\t\t</div>
\t\t</div>
\t\t<div class=\"row\">
\t\t\t<form method=\"POST\" action=\"\">
\t\t\t\t<input type=\"text\" name=\"username\" id=\"username\" placeholder=\"";
        // line 14
        echo ($context["input_phone"] ?? null);
        echo "\" />
\t\t\t\t<input type=\"password\" name=\"password\" id=\"password\" placeholder=\"";
        // line 15
        echo ($context["input_password"] ?? null);
        echo "\" />
\t\t\t\t<button>";
        // line 16
        echo ($context["login_button"] ?? null);
        echo "</button>
\t\t\t\t<p style=\"text-align:right;font-size:12px;\">";
        // line 17
        echo ($context["forgot_password"] ?? null);
        echo " <strong>";
        echo ($context["reset_password"] ?? null);
        echo "</strong></p>
\t\t\t\t<p class=\"error-msg\" id=\"error_container\">";
        // line 18
        if (($context["message"] ?? null)) {
            echo ($context["message"] ?? null);
        }
        echo "</p>
\t\t\t</form>
\t\t</div>
\t</div>
";
        // line 22
        echo ($context["footer_askphoenix"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/account/login.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 22,  74 => 18,  68 => 17,  64 => 16,  60 => 15,  56 => 14,  44 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/account/login.twig", "");
    }
}

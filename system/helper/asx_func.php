<?php
class Askphoenix {

    public function asx_check_token($check_token) {
        $results = array();
        if(empty($check_token)) {
            $results['asx_redirect'] = 'account/login'; //redirect user to login page
        }
        else { //token exists - send parameters
            $results['asx_redirect'] = '';
            $results['asx_token'] = $check_token;
        }
        return $results;
    }

    public function asx_curl_login_user($url, $data, $redirect_to) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                'Content-Type:application/json'
                //'Content-Length: ' . strlen($data)
            )
        );

        $json = curl_exec($curl);
        $curl_error = curl_error($curl);
        $curl_errno = curl_errno($curl);

        if (!$json) { //error
            //echo "cURL Error ($curl_errno): $curl_error\n";
            curl_close($curl);
        } else { //success
            $results_array = array();
            $header = curl_getinfo($curl); //404 or 200 = $header['http_code']
            $results = json_decode($json);
            //get token
            if ($header['http_code'] == 200 && !empty($results->token)) {
                $results_array['asx_http_code'] = 200;
                $results_array['asx_token'] = $results->token;
                $results_array['asx_redirect'] = $redirect_to;
            } elseif ($results->code == 1 || !empty($results->message)) {
                $results_array['asx_code'] = $results->code;
                $results_array['asx_message'] = $results->message;
            }
            curl_close($curl);
            return $results_array;
        }
    }

    public function asx_curl_post($url, $data, $args, $redirect_to) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                'Content-Type:application/json',
                'Authorization: Bearer ' . $args['token']
                //'Content-Length: ' . strlen($data)
            )
        );

        $json = curl_exec($curl);
        $curl_error = curl_error($curl);
        $curl_errno = curl_errno($curl);

        if (!$json) { //error
            //echo "cURL Error ($curl_errno): $curl_error\n";
            curl_close($curl);
        } else { //success
            $results_array = array();
            $header = curl_getinfo($curl); //404 or 200 = $header['http_code']
            $results = json_decode($json);
            if ($header['http_code'] == 200) {
                $results_array['asx_http_code'] = 200;
                $results_array['results'] = $results;
                $results_array['asx_redirect'] = $redirect_to;
            } elseif ($results->code == 1 || !empty($results->message)) {
                $results_array['asx_code'] = $results->code;
                $results_array['asx_message'] = $results->message;
            }
            curl_close($curl);
            return $results_array;
        }
    }

    public function asx_curl_delete($url, $data = '', $args, $redirect_to) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL             =>  $url,               //'https://api.askphoenix.gr/api/exam/11',
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => '',
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 0,
            CURLOPT_FOLLOWLOCATION  => true,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => 'DELETE',
            CURLOPT_HTTPHEADER      => array(
                'Authorization: Bearer ' . $args['token']
            ),
        ));

        $json = curl_exec($curl);
        $curl_error = curl_error($curl);
        $curl_errno = curl_errno($curl);
//        $code_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
//        echo $code_status;
        if ($curl_errno > 0) { //error !$json
            //echo "cURL Error ($curl_errno): $curl_error\n";
            curl_close($curl);
        } else { //success
            $results_array = array();
            $header = curl_getinfo($curl); //404 or 200 = $header['http_code'] //DELETE method returns 204
            $results = json_decode($json);
            if ($header['http_code'] == 200 || $header['http_code'] == 204) {
                $results_array['asx_http_code'] = 204;
                $results_array['results'] = $results;
                $results_array['asx_redirect'] = $redirect_to;
            } elseif ($results->code == 1 || !empty($results->message)) {
                $results_array['asx_code'] = $results->code;
                $results_array['asx_message'] = $results->message;
            }
            curl_close($curl);
//            print_r($json);
//            print_r($results_array);
            return $results_array;
        }
    }

    public function asx_general_curl($url, $data, $args, $redirect_to) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS  => (!empty($data)) ? $data : '',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $args['token']
            ),
        ));

        $response = curl_exec($curl);
//        $curl_error = curl_error($curl);
//        $curl_errno = curl_errno($curl);
        if (!$response) { //error
//            echo "cURL Error ($curl_errno): $curl_error\n";
            curl_close($curl);
        } else { //success
            $results_array = array();
            $header = curl_getinfo($curl); //404 or 200 = $header['http_code']
            $results = json_decode($response);

            if ($header['http_code'] == 200) {
                $results_array['asx_http_code'] = 200;
                $results_array['results'] = $results;
                $results_array['asx_redirect'] = $redirect_to;
            } elseif ($results->code == 1 || !empty($results->message)) {
                $results_array['asx_code'] = $results->code;
                $results_array['asx_message'] = $results->message;
            }
            curl_close($curl);
            return $results_array;
        }
    }

    public function asx_school_information() {

    }

    public function asx_notices($msg) {
        return '<span class="asx_notices">'.$msg.'</span>';
    }

    public function unsetSessionsExams() {
        unset($this->session->data['course_id']);
        unset($this->session->data['course_name']);
        unset($this->session->data['date_id']);
        unset($this->session->data['lecture_id']);
        unset($this->session->data['date_format']);
        unset($this->session->data['class_room_id']);
    }
}
